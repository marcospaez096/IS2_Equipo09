from time import strptime
from django import  forms
from django.contrib.admin import models
from django.utils.functional import keep_lazy

from gestionarproyecto.models import Proyectos
from gestionarsprint.models import relacion_sprint_user_rol
from .models import *

class formulario_crear_us(forms.ModelForm):
    nombre = forms.CharField(max_length=100,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcioncorta = forms.CharField(label='Descripcion Corta',max_length=100,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcionlarga = forms.CharField(label='Descripcion Larga',max_length=300,required=False,widget=forms.Textarea(attrs={'class': 'form-control','rows':'3'}))
    tiempoestimado = forms.IntegerField(label='Tiempo Estimado (horas)',required=True,min_value=1,widget=forms.NumberInput(attrs={'class':'form-control'}))
    prioridad = forms.IntegerField(label='Prioridad (1 al 10)',required=True,min_value=1,max_value=10,widget=forms.NumberInput(attrs={'class':'form-control'}))

    class Meta:
        model =  UserStory
        fields = ['nombre','descripcioncorta','descripcionlarga','tiempoestimado','criterios','prioridad','tipous']

    def __init__(self, *args, **kwargs):
        self.proyecto_nombre_url = kwargs.pop('proyecto_nombre_url')
        objetoproyecto = Proyectos.objects.get(nombreproyecto=self.proyecto_nombre_url)
        super(formulario_crear_us, self).__init__(*args, **kwargs)
        self.fields['tipous'] = forms.ModelChoiceField(queryset=gestiontipoUS.objects.filter(proyecto=objetoproyecto.codigoproyecto), label='Tipo de User Story')
