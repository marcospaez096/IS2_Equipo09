from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User

from tiposUS.models import gestiontipoUS
from gestionarsprint.models import Sprint

# Create your models here.
class UserStory(models.Model):
    #nombre representa el nombre del Us , es unico en toda la bd
    nombre= models.CharField(max_length=100)
    #descripcion corta, representa una descripcion breve del us
    descripcioncorta = models.CharField(max_length=100)
    #descripcion larga, representa una descripcion larga del us
    descripcionlarga = models.CharField(max_length=300)
    #criterios, representa los criterios con el cual sera juzgado un us en el estado "Hecho"
    criterios = ArrayField(models.CharField(max_length=100), null=True)
    #prioridad , representa la prioridad con la cual sera hecha el us , tiene un valor de 1 al 10
    prioridad = models.IntegerField(default=0)
    #SprintDefinido para saber en que sprint esta
    sprintdefinido= models.ForeignKey(Sprint, on_delete=False,null=True)
    #tiempoestimado, tiempo el cual se estima que sera finalizado el us
    tiempoestimado=models.IntegerField(default=0)
    #tiempotrabajado, tiempo en el cual un us fue desarrollado
    tiempotrabajado=models.IntegerField(default=0)
    #tipous, representa el tipodeus al cual el us esta asociado
    tipous= models.ForeignKey(gestiontipoUS, on_delete=False)
    #representa un us al cual un usuario fue asignado
    usuarioasignado=models.ForeignKey(User, on_delete=False,null=True)
    #representa, el estado de un us , activo, finalizado,rechazado
    estado=models.CharField(max_length=100, null=True)
    #productbacklog, booleano que representa si el us esta o no en un productbacklog
    productbacklog = models.BooleanField(default=True)
    #estadokanban,esta en el cual se encuentra un us dentro de un flujo
    estadokanban = models.CharField(max_length=100)
    subestadokanban = models.CharField(max_length=100)
    motivorechazo = models.CharField(max_length=100,null=True)
    def __str__(self):
        return self.nombre

class progresoususer(models.Model):

    usuarioacargo = models.ForeignKey(User,null=True,on_delete=False)
    sprint = models.ForeignKey(Sprint,on_delete=False,null=True)
    us = models.ForeignKey(UserStory,null=True,on_delete=False)
    #horastrabajadas en el dialaboral 0, dialaboral 1 , ... dentro del sprint en el US
    horastrabajadas = models.IntegerField(default=0)
    #comienza con 0, desde el momento en que se comienza el sprint ,  y avanza secuencialmente 0 , 1 , 2, 3 hasta ser igual a la duracion del sprint
    dialaboral = models.CharField(max_length=100,null=True)
    historia = models.CharField(max_length=300,null=True)
    #activo,inactivo,finalizado
    status = models.CharField(max_length=100)
    horasutilizadasdelsprint = models.IntegerField(default=0)
