from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^crearus', views.redireccionar_a_crear_us, name='crearus'),
    url(r'^modificarus/(?P<id_us>[\w\s]+)',views.modificarus,name='modificarus'),
    url(r'^productbacklog/', views.productbacklogbview, name='urlpblog'),
    #url(r'^buscarus',views.redireccionar_a_buscar_us,name="buscarus"),
    #url(r'^eliminarus', views.redireccionar_a_eliminar_us, name='eliminarus'),
    #url(r'^submit_eliminar_us', views.submit_eliminar_us, name='submit_eliminar_us'),
    #url(r'^modificar_us',views.redireccionar_modificar_us,name="modificar_us"),
    #url(r'^submit_modificar_us',views.submit_modificar_us,name="submit_modificar_us")
    #url(r'^visualizar_us',views.visualizar_us,name="visualizar_us")
]

