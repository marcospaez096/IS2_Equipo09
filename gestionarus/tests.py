import unittest

from django.test import TestCase
from django.contrib.auth.models import User
from gestionarproyecto.models import Proyectos
from gestionarus.models import UserStory
from tiposUS.models import tipoUS, gestiontipoUS
from gestionarFlujos.models import Flujo, Estado
from gestionarsprint.models import Sprint
from datetime import *

class SimplePrueba(unittest.TestCase):
    def test_crearsprint(self):
        User.objects.create_user(password='contrasena', username='usuario1', email='email@email.com', first_name='nombres',
                                 last_name='apellidos').save()
        Proyectos(nombreproyecto='nombreproyecto', codigoproyecto='11', anoasociado='2018', fechainicio='2018-03-12',
                  fechafinalizacion='2018-05-05', duracionsprint='15',
                  administrador_id=User.objects.get(username='usuario1')).save()
        Flujo(nombre='flujo1').save()
        subestados= ['Hacer','Haciendo','Hecho']
        proyectoactual= Proyectos.objects.get(nombreproyecto='nombreproyecto')
        flujo= Flujo.objects.get(nombre='flujo1')

        gestiontipoUS(nombretipus='tipo1',flujo=flujo,proyecto=proyectoactual, estado=['Estado1'],subestado=subestados).save()
        idtipous = gestiontipoUS.objects.get(nombretipus='tipo1',flujo='flujo1')
        UserStory.objects.create(nombre='us1', descripcioncorta='descripcioncorta', descripcionlarga='descripcionlarga',
                                 tipous=idtipous)
