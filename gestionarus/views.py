from django.shortcuts import render, redirect, get_object_or_404
from gestionarproyecto.models import *
from gestionarsprint.models import relacion_sprint_user_rol
from gestionarus.forms import formulario_crear_us
from tiposUS.models import gestiontipoUS
from gestionarus.models import UserStory
from datetime import *

from .models import *
from django.utils import *

def productbacklogbview(request,usuario_nombre,proyecto_nombre_url):
    #se verifica si existe un envio de metodos get, y si existen 3 parametros como us_id y sprint_id y usuario_asignado

    if request.method == "GET" and request.GET.get('us_id') and request.GET.get('sprint_id') and request.GET.get('usuario_asignado') and request.GET.get('horaslaboralesdiarias'):
        #obtener el us con id igual a los datos enviados por medio de get  us_id
        us = UserStory.objects.get(pk=request.GET.get('us_id'))
        sprintactual  = Sprint.objects.get(sprint_id=request.GET.get('sprint_id'))

        #se asigna el atributo sprintdefinido del us obtenido y se le asigna el valor sprint_id obtenido por get
        us.sprintdefinido = Sprint.objects.get(sprint_id=request.GET.get('sprint_id'))
        #se asigna el atributo productbacklog a false , ya que fue asignado a un sprint
        us.productbacklog = False
        us.estado="nocomenzado";



        #se asigna el atribtuto usuario asignado
        us.usuarioasignado = User.objects.get(username=request.GET.get('usuario_asignado'))
        #se guarda el us
        us.save()

        r = relacion_sprint_user_rol.objects.get(sprint_id= request.GET.get('sprint_id'),member=relacion_rol_proyecto_usuario.objects.get(proyecto=Proyectos.objects.get(nombreproyecto=proyecto_nombre_url),user=User.objects.get(username=request.GET.get('usuario_asignado'))))
        r.horaslaboralesdiarias = request.GET.get('horaslaboralesdiarias')
        #Anadido
        r.capacidadtotal = int(Sprint.objects.get(sprint_id= request.GET.get('sprint_id')).duracion)*int(r.horaslaboralesdiarias)
        r.save()
        id_sprint = request.GET.get('sprint_id')
        sprintmembers = relacion_sprint_user_rol.objects.filter(sprint_id=id_sprint).all()
        sprintactual = Sprint.objects.get(sprint_id=id_sprint)
        capacidadsprint = 0
        for member in sprintmembers:
            capacidadsprint += member.horaslaboralesdiarias

        capacidadsprint *= int(sprintactual.duracion)
        sprintactual.capacidadtotalsprint = capacidadsprint
        sprintactual.save()
        print(capacidadsprint)
        sprintactual.capacidadtotalsprint = capacidadsprint
        sprintactual.horasrestantes = capacidadsprint
        sprintactual.save()
        #Si dialaboral =-1 , entonces este progreso se creo en respuesta a la asignacion de un us
        nuevoprogreso = progresoususer.objects.create()
        nuevoprogreso.usuarioacargo = User.objects.get(username=request.GET.get('usuario_asignado'))
        nuevoprogreso.us = UserStory.objects.get(id=request.GET.get('us_id'))
        nuevoprogreso.status = 'nocomenzado'
        nuevoprogreso.historia = 'Creación del User Story'
        nuevoprogreso.sprint = Sprint.objects.get(sprint_id = request.GET.get('sprint_id'))
        nuevoprogreso.save()


    proyecto = Proyectos.objects.get(nombreproyecto = proyecto_nombre_url)
    allus=UserStory.objects.filter().order_by('-prioridad')

    allsprint = Sprint.objects.filter(proyecto_id=Proyectos.objects.get(nombreproyecto=proyecto_nombre_url))
    allsprintmember = relacion_sprint_user_rol.objects.filter(sprint__proyecto_id=Proyectos.objects.get(nombreproyecto=proyecto_nombre_url))
    print(allus)


    return render(request,'gestionarus/ProductBacklog.html',{"username":request.user.username,"proyecto":proyecto_nombre_url,'us':allus,"sprint":allsprint,"allsprintmember":allsprintmember})




def redireccionar_a_crear_us(request,usuario_nombre,proyecto_nombre_url):
    """

    <p>Redireccionar a crear us</p>
    <p>Primero se obtiene el username de la url , como asi tambien el codigoproyecto, luego se hace un query a la base de datos de donde se extrae el proyecto de que coincida con el
    codigoproyecto que fue pasado a la url, y luego se redirecciona al template de crearus, pasandole variable como el username y el objeto proyecto
    </p>
    """
    username = usuario_nombre
    proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
    if request.method == 'POST':
        formulario = formulario_crear_us(request.POST,proyecto_nombre_url=proyecto_nombre_url)
        if formulario.is_valid():
            formulario.save()
            return redirect('urlpblog', usuario_nombre, proyecto_nombre_url)

    formulario = formulario_crear_us(proyecto_nombre_url=proyecto_nombre_url)
    return render(request,'gestionarus/crearus.html',{"username":username,"proyecto":proyecto,"formulario":formulario})


def modificarus(request,id_us,proyecto_nombre_url,usuario_nombre):
    username = usuario_nombre
    proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
    #se verificar si el metodo es POST
    if request.method == 'POST':
        #se obtiene los datos del us cuyo id es igual al pasado como parametro al view y se los carga al form
        instancia =  get_object_or_404(UserStory, id=id_us)
        #se crea un objeto formulario y se le pasan los datos
        formulario = formulario_crear_us(request.POST, proyecto_nombre_url=proyecto_nombre_url,instance=instancia)
        #si se envia el formulario y los datos son validos
        if formulario.is_valid():
            #guardar los datos en la bd
            formulario.save()
            #redireccionar al product backlog
            return redirect('urlpblog', usuario_nombre, proyecto_nombre_url)
    # se obtiene los datos del us cuyo id es igual al pasado como parametro al view y se los carga al form
    instancia = get_object_or_404(UserStory, id=id_us)
    # se crea un objeto formulario y se le pasan los datos

    formulario = formulario_crear_us(proyecto_nombre_url=proyecto_nombre_url,instance=instancia)
    return render(request, 'gestionarus/crearus.html',
                  {"username": username, "proyecto": proyecto, "formulario": formulario})
