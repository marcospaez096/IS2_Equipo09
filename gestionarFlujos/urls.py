from django.conf.urls import include, url
from django.contrib import admin

import gestionarproyecto
from  gestionarFlujos import views

app_name = 'gestionarflujos'

urlpatterns = [

    url(r'^agregar/', views.CreaFlujosView.as_view(), name='crearflujos'),
    url(r'^(?P<detailflujos_url>\w+)$', views.FlujosDetailView.as_view(), name='detailtflujos'),
    url(r'^estados/(?P<detailflujos_url>\w+)/(?P<detailestado_url>\w+)$', views.EstadoDetailView.as_view(), name='detailestado'),

]