from django.db import models
from django.contrib.postgres.fields import ArrayField
from gestionarproyecto.models import  Proyectos

# Se crea el modelo flujo con atributos.
class Flujo(models.Model):
    #atributo que indica el nombre del flujo que sera relacionado con varios estados
    nombre= models.CharField(max_length=100,unique=True)

    def __str__(self):
        return self.nombre

# Se crea el modelo estado con atributos.
class Estado(models.Model):
    #atributo que indica el alcance del estado dentro de un proyecto
    proyecto=models.ForeignKey(Proyectos,on_delete=False,blank=True, null=True)
    #atributo que indica  a que flujo pertenece el estado
    flujo = models.ForeignKey('Flujo',db_column='nombre', on_delete=False)
    #atributo nombre del estado
    estado=models.CharField(max_length=100)
    # atributo que indica los subestados de un Estado
    subestado = ArrayField(models.CharField(max_length=100, unique=True))

    class Meta:
        # solo se permite un estado con el mismo nombre dentro de un proyecto
        #relacionado con un flujo
        unique_together = (('proyecto','flujo', 'estado'))

    def __str__(self):
        return self.estado

