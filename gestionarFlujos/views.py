from django.shortcuts import render
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView

from gestionarFlujos.models import Flujo,Estado
from gestionarproyecto.models import  Proyectos
from gestionarFlujos.forms import CreateFlujoForm,CreateEstadoForm
from django.shortcuts import get_object_or_404


from django.http import HttpResponseRedirect
from django.urls import reverse_lazy,reverse
from django.shortcuts import render


# Vista que muestra los detalles de un flujo
class FlujosDetailView(DetailView):
    # nombre del modelo a mostrar
    model = Flujo
    # template a mostar el FLujo
    template_name = 'detailFlujos.html'
    # nombre del campo en la bd de donde buscar
    slug_field = 'nombre'
    # nombre del flujo  a traer el objeto
    # el nombre se obtiene de la url
    slug_url_kwarg = 'detailflujos_url'


#Vista donde  crear un Flujo
class CreaFlujosView(CreateView):
    #template donde mostrar el formulario
    template_name = 'addFlujos.html'
    #modelo del objeto a crear
    model = Flujo
    #formulario para mostrar los campos del modelo flujo
    form_class = CreateFlujoForm


    def get_context_data(self, **kwargs):
        context = super(CreaFlujosView, self).get_context_data(**kwargs)
        context['username'] = self.request.GET.get('usuario')
        return context

    #se valida el formulario
    def form_valid(self, form,**kwargs):
        #se trae el objeto Flujo del formulario

        Objectflujo=form.save(commit=False)
        #se enlaza el flujo objeto a un proyecto
        Objectflujo.proyecto = get_object_or_404(Proyectos, nombreproyecto=self.kwargs.get('proyecto_nombre_url'))
        # se guarda el objeto flujo
        Objectflujo.save()
        context = super(CreaFlujosView, self).get_context_data(**kwargs)
        context['username'] = self.request.GET.get('usuario')

        return HttpResponseRedirect(reverse('tiposUS:creartiposus',
                                            kwargs={'proyecto_nombre_url': self.kwargs.get('proyecto_nombre_url'),'usuario_nombre': self.kwargs.get('usuario_nombre')},
                                            current_app='tiposUS') + '?usuario=' + context['username'])



# Vista que muestra formulario para crear de un Estado

class EstadoDetailView(DetailView):
    # nombre del modelo a mostrar
    model = Estado
    # template a mostar estado
    template_name = 'detailEstado.html'
    #campo de la base de datos
    slug_field = 'estado'
    #nombre del estado a traer el objeto
    slug_url_kwarg = 'detailestado_url'

    #se filtra todos los estados de un flujo
    def get_queryset(self,**kwargs):
        #query traer todos los objetos estados con un flujo espefico
        flujocons=get_object_or_404(Flujo,nombre=self.kwargs.get('detailflujos_url'))
        #se retorna los resultados a un template
        return Estado.objects.filter(flujo=flujocons, estado=self.kwargs.get('detailestado_url'))
