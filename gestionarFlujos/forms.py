from django import forms
from gestionarFlujos.models import Flujo,Estado

class CreateFlujoForm(forms.ModelForm):
    class Meta:
        model=Flujo
        fields = '__all__'

    nombre = forms.CharField(max_length=15,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))

class CreateEstadoForm(forms.ModelForm):
    class Meta:
        model=Estado
        fields =['estado', 'subestado']



