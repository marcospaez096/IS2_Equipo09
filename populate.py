import os



def populate():

    #Creacion de User
    createuser('arturo','Arturo','Mendoza','arturo689@gmail.com','politecnica',False)
    createuser('norio', 'Norio','Shimomoto','noriojapan@gmail.com','shimomoto',False)
    createuser('carlos', 'Carlos','Cabrera','cadacaes@gmail.com','12345',True)
    createuser('marcos', 'Marcos','Paez','marcos@gmail.com','marmaduke',True)
    createuser('ariel', 'Ariel','Mendieta','ariel@gmail.com','marmaduke',False)
    createuser('daniel', 'Daniel','Esteche','daniel@gmail.com','marmaduke',False)

    #Creacion de Roles
    createroles('ScrumMaster',"crear,modificar,visualizar,eliminar","puede crear,puede modificar,puede visualizar,puede eliminar","gestionarproyecto.proyectos")
    createroles('Developer',"visualizar","puede visualizar",  "gestionarproyecto.proyectos")
    createroles('Product Owner',"crear,visualizar","puede crear,puede visualizar",  "gestionarproyecto.proyectos")

    #Creacion de Proyecto
    createproyecto('ProyectoFP_UNA','2018','2018-05-24','2019-05-24',10,'carlos')
    createproyecto('ProyectoIS2','2018','2018-05-24','2019-05-24',10,'marcos')
    createproyecto('ProyectoFinalizar','2018','2018-05-24','2018-06-24',10,'arturo')

    #Creacion de Rol Proyecto Usuario
    createproyectorolusuario('ScrumMaster','arturo','ProyectoIS2')
    createproyectorolusuario('Developer','dani','ProyectoIS2')
    createproyectorolusuario('Developer','carlos','ProyectoIS2')
    createproyectorolusuario('Developer','norio','ProyectoIS2')
    createproyectorolusuario('ScrumMaster','marcos','ProyectoFP_UNA')
    createproyectorolusuario('Developer','ari','ProyectoFP_UNA')
    createproyectorolusuario('ScrumMaster','carlos','ProyectoFinalizar')
    createproyectorolusuario('Developer','marcos','ProyectoFinalizar')
    createproyectorolusuario('Developer','arturo','ProyectoFinalizar')


    #Creacion de tipoUS
    createtipous('tipous1','flujo10','ProyectoFP_UNA','{Estado1,Estado2}')
    createtipous('tipous2','flujo50','ProyectoIS2','{Analisis,Diseño,Desarrollo,Prueba}')
    createtipous('tipous3','flujo3','ProyectoFinalizar','{EstadoInicio,EstadoProceso,EstadoFin}')

    #Creacion de Sprint
    createsprint('Sprint4','2018-02-24',None,'20','ProyectoFP_UNA')
    createsprint('Sprint2','2018-03-03',None,'15','ProyectoFP_UNA')
    createsprint('Sprint3','2018-02-02',None,'15','ProyectoFP_UNA')
    createsprint('Sprint1','2018-02-02','10','10','ProyectoFinalizar')

    createmembersprint('Sprint1', 'carlos', '10')
    createmembersprint('Sprint1', 'marcos', '8')
    createmembersprint('Sprint2', 'marcos', '8')


    #Creacion de User Story
    createus('us1', 'descripcioncorta', 'descripcionlarga', '{criterio1,criterio2}', '8', 'Sprint3', '120','0',
             'tipous1', 'marcos', 'activo', 'False', 'Estado2', 'Haciendo',None)
    createus('us1', 'descripcioncorta', 'descripcionlarga', '{criterio1,criterio2}', '8', 'Sprint2', '120','0',
             'tipous1', 'marcos', 'activo', 'False', 'Estado1', 'Haciendo',None)
    createus('us1', 'descripcioncorta', 'descripcionlarga', '{criterio1,criterio2}', '8', 'Sprint4', '120','0',
             'tipous1', 'marcos', 'activo', 'False', 'Estado1', 'Haciendo',None)
    createus('us1fin', 'descripcioncorta', 'descripcionlarga', '{criterio1,criterio2}', '8', 'Sprint1', '120','0',
             'tipous3', 'carlos', 'activo', 'False', 'EstadoFin', 'Hecho',None)
    createus('us2fin', 'descripcioncorta', 'descripcionlarga', '{criterio1,criterio2}', '8', 'Sprint1', '120','0',
             'tipous3', 'marcos', 'activo', 'False', 'EstadoFin', 'Hecho',None)
    createus('us3fin', 'descripcioncorta', 'descripcionlarga', '{criterio1,criterio2}', '8', 'Sprint1', '120','0',
             'tipous3', 'arturo', 'activo', 'False', 'EstadoFin', 'Hecho',None)
    createus('us2', 'descripcioncorta', 'descripcionlarga', '{criterio1,criterio2}', '8', None, '120','0',
             'tipous2', None, None, 'True', '','',None)



def createus(nombre,descripcioncorta,descripcionlarga,criterios,prioridad,nombresprint,tiempoestimado,tiempotrabajado,
             nombretipous,nombreusuario,estado,productbacklog,estadokanban,subestadokanban,motivorechazo):
    progresodiario = progresoususer.objects.create()
    if nombresprint:
        sprintdefinido = Sprint.objects.get(nombresprint=nombresprint)
        progresodiario.dialaboral = Sprint.objects.get(sprint_id=sprintdefinido.sprint_id).diaactual
        progresodiario.sprint = Sprint.objects.get(sprint_id=sprintdefinido.sprint_id)
    else:
        sprintdefinido = None
    if nombretipous:
        tipous = gestiontipoUS.objects.get(nombretipus=nombretipous)
    else:
        tipous = None
    if nombreusuario:
        usuarioasignado = User.objects.get(username=nombreusuario)
        progresodiario.usuarioacargo = User.objects.get(username=usuarioasignado.username)
    else:
        usuarioasignado = None
    us = UserStory.objects.create(nombre=nombre,descripcioncorta=descripcioncorta,descripcionlarga=descripcionlarga,
                             criterios=criterios,prioridad=prioridad,sprintdefinido=sprintdefinido,
                             tiempoestimado=tiempoestimado,tiempotrabajado=tiempotrabajado,tipous=tipous,
                             usuarioasignado=usuarioasignado,estado=estado,productbacklog=productbacklog,
                             estadokanban=estadokanban,subestadokanban=subestadokanban,motivorechazo=motivorechazo)


    progresodiario.us = UserStory.objects.get(id=us.id)
    progresodiario.horastrabajadas = us.tiempotrabajado
    progresodiario.historia = 'Estado: ' + us.estadokanban + ' - Subestado: ' + us.subestadokanban + \
                              '- Horas Trabajadas: ' + us.tiempotrabajado + ' - Anotaciones: ' + 'Creacion de US'
    # Se trabajo una cantidad de horastrabajadas en un dialaboral

    if us.subestadokanban == "Hecho":
        us.estado = "estadodeaceptacion"
        progresodiario.status = "estadodeaceptacion"
    else:
        progresodiario.status = "activo"
    progresodiario.save()

def createtipous(nombre,flujo,proyecto,estado):
    proyecto=Proyectos.objects.get(nombreproyecto=proyecto)

    gestiontipoUS.objects.create(nombretipus=nombre,flujo =flujo,proyecto =proyecto,estado=estado,subestado='{Hacer,Haciendo,Hecho}')

def createproyecto(nombre,ano,inicio,finalizacion,duracion,administrador):
    admin=User.objects.get(username=administrador)
    Proyectos.objects.create(nombreproyecto=nombre,anoasociado=ano,fechainicio=inicio,fechafinalizacion=finalizacion,duracionsprint =duracion,administrador_id=admin)

def createuser(username,nombre,apellido,correo,password,super):
    User.objects.create_user(username=username,first_name=nombre,last_name=apellido,email=correo,password=password,is_superuser = super)


def createsprint(nombre,fechainicio,diaactual,duracion,proyecto):
    idproyecto = Proyectos.objects.get(nombreproyecto=proyecto)
    if diaactual:
        Sprint.objects.create(nombresprint=nombre,diaactual=diaactual,fechainicio=fechainicio, duracion=duracion, proyecto_id=idproyecto)
    else:
        Sprint.objects.create(nombresprint=nombre,fechainicio=fechainicio,duracion=duracion,proyecto_id=idproyecto)

def createroles(namegroup,permisos,detailpermisos,ambito):
    nuevogrupo = Group.objects.create(name=namegroup)
    array_string_permisos = permisos.split(",")
    array_string_descripcionpermisos = detailpermisos.split(",")
    ambitopermiso = ambito.split(".")
    modelo = apps.get_model(ambitopermiso[0], ambitopermiso[1])
    for i in range(len(array_string_permisos)):
        if Permission.objects.filter(codename=array_string_permisos[i]).exists():
            nuevogrupo.permissions.add(Permission.objects.get(codename=array_string_permisos[i]))
        else:
            permission = Permission.objects.create(codename=array_string_permisos[i],
                                               name=array_string_descripcionpermisos[i],
                                               content_type=ContentType.objects.get_for_model(modelo))
            nuevogrupo.permissions.add(Permission.objects.get(codename=array_string_permisos[i]))

def createproyectorolusuario(nombrerol,nombreuser,nombreproyecto):
    rol = Group.objects.get(name=nombrerol)
    user = User.objects.get(username=nombreuser)
    proyecto= Proyectos.objects.get(nombreproyecto=nombreproyecto)
    relacion_rol_proyecto_usuario.objects.create(rol=rol,user=user,proyecto=proyecto)
    for i in rol.permissions.all():
        assign_perm(i, user, proyecto)

def createmembersprint(nombresprint,nombremember,horas):
    sprint = Sprint.objects.get(nombresprint=nombresprint)
    proyecto = sprint.proyecto_id
    user = User.objects.get(username=nombremember).id
    member = relacion_rol_proyecto_usuario.objects.get(proyecto=proyecto,user=user)
    relacion_sprint_user_rol.objects.create(sprint=sprint,member=member,horaslaboralesdiarias=horas)
# AQUI empieza la ejecucion
if __name__ == '__main__':
    print ("Inicio de la poblacion de la base de datos")
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'IS2_SGP.produccion_settings')
    import django

    django.setup()

    from django.contrib.auth.models import User, Group,Permission
    from django.utils import timezone
    from datetime import timedelta
    from django.contrib.contenttypes.models import ContentType
    from django.apps import apps
    from gestionarproyecto.models import *
    from tiposUS.models import gestiontipoUS
    from gestionarsprint.models import Sprint, relacion_sprint_user_rol
    from gestionarus.models import  *
    from guardian.shortcuts import *

    populate()
