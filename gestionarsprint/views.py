from aptdaemon.lock import status_lock
from django.db.models import CharField
from django.shortcuts import render, redirect, get_object_or_404
# from orca.orca_console_prefs import sayAndPrint
from django.core.mail import EmailMessage
from gestionarproyecto.models import *
from datetime import *

from gestionarsprint.forms import formulario_sprint_members, formulario_sprint
from gestionarus.models import UserStory, progresoususer
from .models import *
from gestionarFlujos.models import *
from tiposUS.models import *
from django.utils import *


# === Vistas de la app gestionarsprint ===
def visualizar_sprint(request, usuario_nombre, proyecto_nombre_url, id_sprint):

    end_date = Sprint.objects.get(sprint_id=id_sprint).fechainicio + timedelta(days=int(Sprint.objects.get(sprint_id=id_sprint).diaactual))
    #fecha actual convertida a cadena y mostrada en formato dd/mm/yyyy
    cadena = datetime.strftime(end_date, '%d') + "/" + datetime.strftime(end_date, "%m") + "/" + datetime.strftime(
        end_date, "%Y")
    #se inicializan las variables horashechas y horastrabajadashastanatesdehoy
    horashechas = 0
    horastrabajadashastaantesdehoy = 0
    sprint = Sprint.objects.get(sprint_id=id_sprint)
    scrum = relacion_sprint_user_rol.objects.filter(sprint=id_sprint)
    if sprint.diaactual == sprint.duracion:
        for s in scrum:
            email = EmailMessage('Ultimo dia del Sprint','Hoy es el ultimo dia del Sprint segun la planificacion, ya puede finalizar el sprint',to=s.member.user.email)
            email.send()

    #se busca en la tabla progresoususer que busca todos los us asignados al usuario actual y en el dialaboral actual y suma la cantidad de horas trabajadas
    for i in progresoususer.objects.filter(usuarioacargo=User.objects.get(username=usuario_nombre), sprint_id=id_sprint,
                                           dialaboral=Sprint.objects.get(sprint_id=id_sprint).diaactual).all():
        horashechas += int(i.horastrabajadas)

    #se busca en la tabla progresoususer que busca todos los us asignados al usuario actual y hasta antes de la fecha actual para calcular las horas trabajadas en el sprint actual

    for x in progresoususer.objects.filter(usuarioacargo=User.objects.get(username=usuario_nombre), sprint_id=id_sprint,
                                           dialaboral__lt=Sprint.objects.get(sprint_id=id_sprint).diaactual).all():
        horastrabajadashastaantesdehoy += (x.horastrabajadas)
    tracecapacidadtotal = horastrabajadashastaantesdehoy + horashechas
    tracehorashechasdia = horashechas
    #si se recibe una peticion ajax de detenersprint
    if request.method == "GET" and request.GET.get('detenersprint'):
        objsprint = Sprint.objects.get(sprint_id=id_sprint)
        objsprint.status = "terminado"
        objsprint.save()
        #se comienza la clonacion
        for i in UserStory.objects.filter(sprintdefinido_id=id_sprint):
            if i.estado != "finalizado":
                print('---clonacion---')
                print(i)
                i.pk = None
                i.prioridad = 10
                i.sprintdefinido = None
                i.productbacklog = True
                i.save()
    #si se recibe una peticion get de iniciar sprint
    if request.method == 'GET' and request.GET.get('iniciado'):
        objsprint = Sprint.objects.get(sprint_id=id_sprint)
        # empieza a correr el tiempo
        objsprint.diaactual = 0
        objsprint.status = "iniciado"
        objsprint.save()
        ###SE CREA EL VECTOR###
        #donde se almacena los datos del burndownchart
        objsprint.datosgraficos=[]
        objsprint.datosgraficos.insert(0,cadena)
        objsprint.datosgraficos.insert(1,objsprint.capacidadtotalsprint)
        objsprint.save()
    #si se recibe la peticion de avanzar al dia siguiente
    if request.method == 'GET' and request.GET.get('diasiguiente'):
        objsprint = Sprint.objects.get(sprint_id=id_sprint)
        #se aumenta el dia actual
        objsprint.diaactual += 1
        objsprint.save()
        objsprint.datosgraficos.insert(objsprint.diaactual*2, cadena)
        objsprint.datosgraficos.insert(objsprint.diaactual*2+1,objsprint.horasrestantes)

        objsprint.save()
        end_date = objsprint.fechainicio + timedelta(days=int(objsprint.diaactual))
        cadena = datetime.strftime(end_date,'%d')+"/"+datetime.strftime(end_date,"%m")+"/"+datetime.strftime(end_date,"%Y")
        print("--------------")
        #se  verifica si es feriado o domingo
        if es_feriado_o_domingo(cadena):
            #se desactiva si es feriado o domingo
            print("Es feriado o domingo!")
            objsprint =  Sprint.objects.get(sprint_id=id_sprint)
            objsprint.status = "inactivo"
            objsprint.save()

        else:
            #se activa el sprint si no es feriado o domingo
            objsprint.status = "iniciado"
            objsprint.save()
            print("no es feriado ni domingo")


    # Se entra aqui si solo el scrummaster hizo click sobre un User Story en el estado control
    if request.method == 'GET' and request.GET.get('estadofuturo') and request.GET.get(
            'subestadofuturo') and request.GET.get('correcto') and request.GET.get('idus') and request.GET.get(
            'motivo'):
        idus = request.GET.get('idus')
        estadofuturo = request.GET.get('estadofuturo')
        subestadofuturo = request.GET.get('subestadofuturo')
        correcto = request.GET.get('correcto')
        print('---Correcto---')
        print(request.GET.get('correcto'))
        us = UserStory.objects.get(id=idus)
        # activo,cancelado,aprobado
        if correcto == 'True':
            print('Es correcto')
            progresodiario = progresoususer.objects.create()
            progresodiario.usuarioacargo = User.objects.get(username=usuario_nombre)
            progresodiario.us = UserStory.objects.get(id=idus)
            progresodiario.horastrabajadas = 0
            # Se trabajo una cantidad de horastrabajadas en un dialaboral
            progresodiario.dialaboral = cadena
            progresodiario.sprint = Sprint.objects.get(sprint_id=id_sprint)
            if us.estadokanban == us.tipous.estado[-1] and us.subestadokanban == "Hecho":
                progresodiario.status = 'finalizado'
                us.estado = "finalizado"
            else:
                progresodiario.status = 'aprobado'
                us.estado = 'aprobado'

            progresodiario.save()
        else:
            us.motivorechazo = request.GET.get('motivo')
            us.estado = 'rechazado'
            us.estadokanban = estadofuturo
            us.subestadokanban = subestadofuturo
            progresodiario = progresoususer.objects.create()
            progresodiario.usuarioacargo = User.objects.get(username=usuario_nombre)
            progresodiario.us = UserStory.objects.get(id=idus)
            progresodiario.horastrabajadas = 0
            # Se trabajo una cantidad de horastrabajadas en un dialaboral
            progresodiario.dialaboral = cadena
            progresodiario.sprint = Sprint.objects.get(sprint_id=id_sprint)
            progresodiario.status = "rechazado"
            progresodiario.save()
        us.save()
    #si se movio un us en el tablero
    if request.method == 'GET' and request.GET.get('estadoactual') and request.GET.get(
            'subestadoactual') and request.GET.get('idus') and request.GET.get('horastrabajadas') and request.GET.get(
            'notas') and request.GET.get('usuarioasignado'):
        estadoactual = request.GET.get('estadoactual')
        subestadoactual = request.GET.get('subestadoactual')
        horas = request.GET.get('horastrabajadas')
        notas = request.GET.get('notas')
        idus = request.GET.get('idus')
        usuarioasignado = request.GET.get('usuarioasignado')
        dialaboralactual = Sprint.objects.get(sprint_id=id_sprint).diaactual
        horashechas = 0
        horastrabajadashastaantesdehoy = 0
        # horashechas refieren a las horas hechas en el dia actual
        for i in progresoususer.objects.filter(usuarioacargo=User.objects.get(username=usuario_nombre),
                                               sprint_id=id_sprint, dialaboral=dialaboralactual).all():
            horashechas += int(i.horastrabajadas)

        for x in progresoususer.objects.filter(usuarioacargo=User.objects.get(username=usuario_nombre),
                                               sprint_id=id_sprint, dialaboral__lt=dialaboralactual).all():
            horastrabajadashastaantesdehoy += (x.horastrabajadas)

        print('---------horastrabajadasantesdehoy------')
        print(horastrabajadashastaantesdehoy)
        tracecapacidadtotal = horastrabajadashastaantesdehoy + horashechas + int(horas)
        tracehorashechasdia = horashechas + int(horas)

        print('dsfdsfsd')
        print(estadoactual)
        print(subestadoactual)
        print(horas)
        us = UserStory.objects.get(id=idus)
        us.estadokanban = estadoactual
        us.estado = 'activo'
        print('------------')
        print(us.tipous.estado[-1])
        us.subestadokanban = subestadoactual
        us.tiempotrabajado += int(horas)
        sprintactual = Sprint.objects.get(sprint_id=id_sprint)
        sprintactual.horasrestantes -= int(horas)
        sprintactual.save()
        if sprintactual.diaactual == 0:
             sprintactual.datosgraficos[sprintactual.diaactual+1] = sprintactual.horasrestantes
        else:
             sprintactual.datosgraficos[sprintactual.diaactual*2+1] = sprintactual.horasrestantes


        sprintactual.save()
        #if subestadoactual == "Haciendo" or subestadoactual == "Hecho":
        # Se crea un progreso diario
        progresodiario = progresoususer.objects.create()
        progresodiario.usuarioacargo = User.objects.get(username=usuarioasignado)
        progresodiario.us = UserStory.objects.get(id=idus)
        progresodiario.horastrabajadas = horas
        progresodiario.historia = 'Estado: ' + us.estadokanban + ' - Subestado: ' + us.subestadokanban + \
                                  '- Horas Trabajadas: ' + horas + ' - Anotaciones: ' + notas
        # Se trabajo una cantidad de horastrabajadas en un dialaboral
        progresodiario.dialaboral = cadena

        progresodiario.sprint = Sprint.objects.get(sprint_id=id_sprint)


        if subestadoactual == "Hecho":
            us.estado = "estadodeaceptacion"
            progresodiario.status = "estadodeaceptacion"
        else:
            progresodiario.status = "activo"
        progresodiario.save()
        us.save()
        """


    <p>Visualizar sprint</p>
    <p>Primero, se obtiene el nombre de usuario que se pasa por la url.Luego se obtiene el codigodelproyecto,luego se obtiene los tipos de US asociados al proyecto, como asi tambien sus estados, por ultimo reenvia al template de visualizar sprint , pasando los parametros de estados y user story para poder listarlos en los templates
   """

    """Para obtener la capacidad de horas totales del sprint, de acuerdo a las horas disponibles de todos los usuarios"""
    sprintmembers = relacion_sprint_user_rol.objects.filter(sprint_id=id_sprint).all()
    sprintactual = Sprint.objects.get(sprint_id=id_sprint)
    capacidadsprint=0
    for member in sprintmembers:
        capacidadsprint += member.horaslaboralesdiarias

    capacidadsprint *= int(sprintactual.duracion)
    sprintactual.capacidadtotalsprint = capacidadsprint
    sprintactual.save()



    tus = gestiontipoUS.objects.filter(proyecto=Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)).all()
    estado = Estado.objects.filter(proyecto=Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)).all()
    # us_product_backlog = UserStory.objects.filter(productbacklog=True, tipous__proyecto=Proyectos.objects.get(

    diaactual = Sprint.objects.get(sprint_id=id_sprint).diaactual
    try:
        topehoralaboraldiario = relacion_sprint_user_rol.objects.get(sprint_id=id_sprint,
                                                                     member=relacion_rol_proyecto_usuario.objects.get(
                                                                         proyecto=Proyectos.objects.get(
                                                                             nombreproyecto=proyecto_nombre_url),
                                                                         user=User.objects.get(
                                                                             username=usuario_nombre))).horaslaboralesdiarias
        capacidadtotalhoraria = relacion_sprint_user_rol.objects.get(sprint_id=id_sprint,
                                                                     member=relacion_rol_proyecto_usuario.objects.get(
                                                                         proyecto=Proyectos.objects.get(
                                                                             nombreproyecto=proyecto_nombre_url),
                                                                         user=User.objects.get(
                                                                             username=usuario_nombre))).capacidadtotal
    except:
        topehoralaboraldiario = 0
        capacidadtotalhoraria = 0

    progreso = progresoususer.objects.filter(sprint_id=id_sprint, dialaboral__lte=diaactual).all().order_by('-dialaboral')
    # devuelve true si diastranscurridos es menor a la  duraciondelsprint
    # sprintactivo = int(Sprint.objects.get(sprint_id=id_sprint).duracion) > diaactual and Sprint.objects.get(sprint_id=id_sprint).status == "iniciado"
    sprintactivo = Sprint.objects.get(sprint_id=id_sprint).status == "iniciado"
    usall = UserStory.objects.filter(sprintdefinido=Sprint.objects.get(sprint_id=id_sprint))

    username = usuario_nombre
    if request.method == 'POST':
        form = formulario_sprint_members(request.POST, proyecto_nombre_url=proyecto_nombre_url, id_sprint=id_sprint)
        if form.is_valid():
            form.save()
            return redirect('visualizar_sprint', usuario_nombre, proyecto_nombre_url, id_sprint)


    else:
        # codigoproyecto = request.GET.get('proyecto')
        # tus = get_object_or_404(tipoUS, nombre=self.kwargs.get('detailtipous_url'))
        # context['estado'] = Estado.objects.filter(flujo=tus.flujo)
        form = formulario_sprint_members(proyecto_nombre_url=proyecto_nombre_url, id_sprint=id_sprint)
        razon = Sprint.objects.get(sprint_id=id_sprint).capacidadtotalsprint/int(Sprint.objects.get(sprint_id=id_sprint).duracion)
        capacidadprint = Sprint.objects.get(sprint_id=id_sprint).capacidadtotalsprint;
        datosgraficos =Sprint.objects.get(sprint_id=id_sprint).datosgraficos
        matriz = "[\"Dias\",\"Planificado\",\"Realizado\"],"
        for x in range(0,len(datosgraficos)-1):
            if x % 2 ==0:
                fila = "[" + "\"Dia"+str(datosgraficos[x])+"\"" + "," + str(capacidadprint-razon*x if capacidadprint-razon*x >= 0 else 0 ) + "," + str(datosgraficos[x + 1]) + "],"
                matriz += fila
            else:
                continue


        allus = UserStory.objects.filter(sprintdefinido=id_sprint).all();

        return render(request, 'gestionarsprint/visualizarsprint.html',
                      {"username": username, "tus": tus, "estado": estado, "form": form, "usall": usall,
                       "proyecto_nombre_url": proyecto_nombre_url, "id_sprint": id_sprint,
                       "sprintmembers": sprintmembers,
                       "proyectoactual": Proyectos.objects.get(nombreproyecto=proyecto_nombre_url),
                       "diaactual": diaactual, "sprintactivo": sprintactivo,
                       "diasfaltantes": int(Sprint.objects.get(sprint_id=id_sprint).duracion) - diaactual,
                       "progreso": progreso,
                       "horashechas": horashechas,
                       "topelaboraldiario":  topehoralaboraldiario,
                       "capacidadtotalhoraria": capacidadtotalhoraria,
                       "capacidadutilizada": tracecapacidadtotal,
                       "capacidadsprint":capacidadsprint,
                       "matriz": matriz,"fecha" : cadena,"feriado_domingo" : es_feriado_o_domingo(cadena),"allus" : allus,"nombresprint" : Sprint.objects.get(sprint_id=id_sprint).nombresprint})


def redireccionar_a_crear_sprint(request, usuario_nombre, proyecto_nombre_url):
    """

    <p>Redireccionar a crear sprint</p>
    <p>Primero se obtiene el username de la url , como asi tambien el codigoproyecto, luego se hace un query a la base de datos de donde se extrae el proyecto de que coincida con el
    codigoproyecto que fue pasado a la url, y luego se redirecciona al template de crearsprint, pasandole variable como el username y el objeto proyecto
    </p>
    """
    username = usuario_nombre
    proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
    if request.method == 'POST':
        formulario = formulario_sprint(request.POST, proyecto_nombre_url=proyecto_nombre_url)
        formulario.proyecto_id = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
        if formulario.is_valid():
            sprint = formulario.save(commit=False)
            duracion = int(sprint.duracion)
            sprint.fechafinalizacion = sprint.fechainicio + timedelta(duracion)
            proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
            if proyecto.fechainicio:
                if proyecto.fechafinalizacion:
                    proyecto.fechafinalizacion += timedelta(duracion)
                else:
                    proyecto.fechafinalizacion = proyecto.fechainicio + timedelta(duracion)
            proyecto.save()
            sprint.save()
            return redirect('visualizarproyecto', usuario_nombre, proyecto_nombre_url)

    formulario = formulario_sprint(proyecto_nombre_url=proyecto_nombre_url)
    return render(request, 'gestionarsprint/crearsprint.html',
                  {"username": username, "proyecto": proyecto, "formulario": formulario})

def es_feriado_o_domingo(fecha):
    dia_mes_a_buscar = fecha[:5]
    print(dia_mes_a_buscar)
    feriados = ["01/01","01/03","29/03","30/03","01/04",
                    "01/05","15/05","12/06","15/08","29/09","08/12","25/05"]
    if dia_mes_a_buscar in feriados:
        return True
    else:
        #fecha es del tipo string y hay que convertir al tipo date
        print("la fecha es")
        print(fecha)
        conversion_a_fecha_date = datetime.strptime(fecha, '%d/%m/%Y').date()
        print ("La conversion es ")
        print(conversion_a_fecha_date.weekday())
        if conversion_a_fecha_date.weekday() == 6:
            return True
        else:
            return False


def modificar_sprint(request, usuario_nombre, sprint_id, proyecto_nombre_url):
    """

    <p>Redireccionar a crear sprint</p>
    <p>Primero se obtiene el username de la url , como asi tambien el codigoproyecto, luego se hace un query a la base de datos de donde se extrae el proyecto de que coincida con el
    codigoproyecto que fue pasado a la url, y luego se redirecciona al template de crearsprint, pasandole variable como el username y el objeto proyecto
    </p>
    """
    username = usuario_nombre
    proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
    if request.method == 'POST':
        instancia = get_object_or_404(Sprint, sprint_id=sprint_id)
        formulario = formulario_sprint(request.POST, proyecto_nombre_url=proyecto_nombre_url, instance=instancia)

        if formulario.is_valid():
            sprint = formulario.save(commit=False)
            duracion = int(sprint.duracion)
            sprint.fechafinalizacion = sprint.fechainicio + timedelta(duracion)
            sprint.save()
            return redirect('visualizarproyecto', usuario_nombre, proyecto_nombre_url)
    instancia = get_object_or_404(Sprint, sprint_id=sprint_id)
    formulario = formulario_sprint(proyecto_nombre_url=proyecto_nombre_url, instance=instancia)
    return render(request, 'gestionarsprint/modificarsprint.html',
                  {"username": username, "proyecto": proyecto, "formulario": formulario})
