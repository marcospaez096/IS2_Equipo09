from time import strptime
from django import  forms
from django.contrib.admin import models
from django.utils.functional import keep_lazy

from gestionarproyecto.models import relacion_rol_proyecto_usuario
from .models import *

class formulario_sprint(forms.ModelForm):
    nombresprint = forms.CharField(label='Nombre',max_length=100,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    fechainicio = forms.DateField(required=True, widget=forms.TextInput(attrs={'type': 'date', 'class': 'form-control'}))
    class Meta:
        model =  Sprint
        fields = 'nombresprint','fechainicio','duracion','proyecto_id'

    def __init__(self, *args, **kwargs):
        self.proyecto_nombre_url = kwargs.pop('proyecto_nombre_url')
        super(formulario_sprint, self).__init__(*args, **kwargs)
        proyecto = Proyectos.objects.get(nombreproyecto=self.proyecto_nombre_url)
        self.fields['proyecto_id'] = forms.ModelChoiceField(label="Proyecto",queryset=Proyectos.objects.filter(nombreproyecto=self.proyecto_nombre_url))
        self.fields['duracion'] = forms.IntegerField(label='Duracion (dias)',required=True,widget=forms.TextInput(attrs={'class':'form-control','value':proyecto.duracionsprint}))

class formulario_sprint_members(forms.ModelForm):
    horaslaboralesdiarias = forms.IntegerField(label="Horas Laborales Diarias",min_value=1,required=True,widget=forms.NumberInput(attrs={'class':'form-control'}))
    class Meta:
        model = relacion_sprint_user_rol
        fields = ['sprint','member','horaslaboralesdiarias']

    def __init__(self, *args, **kwargs):
        self.proyecto_nombre_url = kwargs.pop('proyecto_nombre_url')
        self.id_sprint = kwargs.pop('id_sprint')
        id_sprint = self.id_sprint
        proyecto_nombre_url = self.proyecto_nombre_url
        super(formulario_sprint_members, self).__init__(*args, **kwargs)
        self.fields['sprint'].queryset = Sprint.objects.filter(sprint_id=self.id_sprint)
        self.fields['member'] = forms.ModelChoiceField(label="Miembros", queryset= relacion_rol_proyecto_usuario.objects.exclude(id__in=relacion_sprint_user_rol.objects.filter(sprint_id=self.id_sprint).values('member')).filter(proyecto=Proyectos.objects.get(nombreproyecto=self.proyecto_nombre_url)))
        #self.fields['user']=  forms.ModelChoiceField(queryset=User.objects.exclude(id__in=relacion_rol_proyecto_usuario.objects.filter(proyecto=Proyectos.objects.get(nombreproyecto=self.proyecto_nombre_url)).values('user_id')),widget=forms.Select(attrs={'class': 'form-control'})
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'