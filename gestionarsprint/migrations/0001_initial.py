# Generated by Django 2.0.2 on 2018-06-14 17:44

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('gestionarproyecto', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='relacion_sprint_user_rol',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horaslaboralesdiarias', models.IntegerField(null=True)),
                ('capacidadtotal', models.IntegerField(default=0)),
                ('member', models.ForeignKey(on_delete=False, to='gestionarproyecto.relacion_rol_proyecto_usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Sprint',
            fields=[
                ('sprint_id', models.AutoField(primary_key=True, serialize=False)),
                ('nombresprint', models.CharField(max_length=100)),
                ('fechainicio', models.DateField(null=True)),
                ('fechafinalizacion', models.DateField(null=True)),
                ('duracion', models.CharField(max_length=100)),
                ('diaactual', models.IntegerField(default=-1)),
                ('feriados', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), null=True, size=None)),
                ('status', models.CharField(default='noiniciado', max_length=100)),
                ('capacidadtotalsprint', models.IntegerField(default=0)),
                ('horasrestantes', models.IntegerField(default=0)),
                ('datosgraficos', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), default=[], size=None)),
                ('proyecto_id', models.ForeignKey(on_delete=False, to='gestionarproyecto.Proyectos')),
            ],
            options={
                'db_table': 'gestionarsprint_sprint',
            },
        ),
        migrations.AddField(
            model_name='relacion_sprint_user_rol',
            name='sprint',
            field=models.ForeignKey(on_delete=False, to='gestionarsprint.Sprint'),
        ),
        migrations.AlterUniqueTogether(
            name='sprint',
            unique_together={('nombresprint', 'proyecto_id')},
        ),
    ]
