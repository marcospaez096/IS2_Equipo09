from django.conf.urls import include, url
from . import views

# === URL DE Gestionar Sprint ===
"""
<p>La url -> crearsprint, redirecciona a la vista llamada redireccionar_a_crear_sprint, la cual crea el sprint,</p>
<p>la url -> submit_crear_sprint redirecciona a la vista llamada submit_crear_sprint , la cual envia el formulario para crear el sprint</p>
<p>La url -> visualizar sprint , redirecciona a la vista llamada visualizar sprint, la cual lista todos los sprint asociados a un proyecto</p>
"""
urlpatterns = [
    url(r'^crearsprint', views.redireccionar_a_crear_sprint, name='crearsprint'),
    url(r'^modificarsprint/(?P<sprint_id>[\w\s]+)', views.modificar_sprint, name='modificarsprint'),
    #url(r'^submit_crear_sprint', views.submit_crear_sprint, name='submit_crear_sprint'),
    #url(r'^buscarsprint',views.redireccionar_a_buscar_sprint,name="buscarsprint"),
    #url(r'^eliminarsprint', views.redireccionar_a_eliminar_sprint, name='eliminarsprint'),
    #url(r'^submit_eliminar_sprint', views.submit_eliminar_sprint, name='submit_eliminar_sprint'),
    #url(r'^modificar_sprint',views.redireccionar_modificar_sprint,name="modificar_sprint"),
    #url(r'^submit_modificar_sprint',views.submit_modificar_sprint,name="submit_modificar_sprint")
    url(r'^visualizar_sprint/(?P<id_sprint>[\w\s]+)',views.visualizar_sprint,name="visualizar_sprint")

]

