from django.contrib.postgres.fields import ArrayField
from django.db import models

# === Modelo Sprint ===
from gestionarproyecto.models import Proyectos, relacion_rol_proyecto_usuario

class Sprint(models.Model):
    """
    sprintid = autoincremento, permite que al crear un sprint el id sea incrementado automaticamente
    nombresprint = columna que permite almacenar los nombres de cada sprint, del tipo cadena
    fechainicio = tipo Date, la columna solo puede almacena datos de tipo fecha
    fechafinalizacion = tipo Date, la columna solo puede almacenar datos de tipo fecha
    duracion = tipo cadena , la columna solo puede almacenar la duracion del sprint
    proyecto_id  = llave foranea a un id proyecto , asi se indica a que proyecto pertenece un sprint

    """
    sprint_id = models.AutoField(primary_key=True)
    nombresprint = models.CharField(max_length=100)
    fechainicio = models.DateField(null = True)
    fechafinalizacion = models.DateField(null = True)
    duracion = models.CharField(max_length=100)
    proyecto_id = models.ForeignKey(Proyectos, on_delete=False)
    #Cuando se oprime el boton iniciarsprint , el diaactual = 0 , comenzo el sprint
    #al oprimir boton "avanzaraldiasiguiente" , diaactual = 1 , 2 , 3 ...
    diaactual = models.IntegerField(default=-1)
    feriados = ArrayField(models.IntegerField(),null=True)
    status = models.CharField(max_length=100,default='noiniciado')
    capacidadtotalsprint = models.IntegerField(default=0)
    horasrestantes= models.IntegerField(default=0)
    datosgraficos = ArrayField(models.CharField(max_length=100),default=[])
    class Meta:
        # nombre de la tabla que será guardada en la base de datos
        db_table = 'gestionarsprint_sprint'
        #se restringe que los sprints tengan el mismo nombre dentro de un proyecto
        unique_together = (('nombresprint', 'proyecto_id'))

    def __str__(self):
        return u'{0}'.format(self.nombresprint,self.proyecto_id)

class relacion_sprint_user_rol(models.Model):
    sprint = models.ForeignKey(Sprint,on_delete=False)
    member = models.ForeignKey(relacion_rol_proyecto_usuario,on_delete=False)
    horaslaboralesdiarias = models.IntegerField(null=True)
    capacidadtotal = models.IntegerField(default=0)
    def __str__(self):
        return u'{0}'.format(self.sprint.sprint_id)