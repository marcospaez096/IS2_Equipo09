from django.test import TestCase
from gestionarproyecto.models import *
# Create your tests here.
import unittest
from django.test import TestCase
#Client simula a un navegador
from django.test.client import Client
from django.contrib.auth.models import User
from gestionarsprint.models import Sprint
from datetime import *

class SimplePrueba(unittest.TestCase):
    def test_crearsprint(self):
        User.objects.create_user(password='contrasena', username='usuario1', email='email@email.com', first_name='nombres',last_name='apellidos').save()
        Proyectos(nombreproyecto='nombreproyecto', codigoproyecto='11', anoasociado='2018', fechainicio='2018-03-12',fechafinalizacion='2018-05-05', duracionsprint='15',administrador_id=User.objects.get(username='usuario1').id).save()
        Sprint(nombresprint='Sprint1',fechainicio='2018-03-12',fechafinalizacion=datetime.strptime('2018-03-12', "%Y-%m-%d").date()+ timedelta(14),duracion=15,proyecto_id_id=Proyectos.objects.get(nombreproyecto='nombreproyecto').codigoproyecto).save()

    def test_crearsprint_error(self):
        User.objects.create_user(password='contrasena', username='usuario2', email='email@email.com', first_name='nombres',last_name='apellidos').save()
        Proyectos(nombreproyecto='nombreproyecto', codigoproyecto='11', anoasociado='2018', fechainicio='2018-03-12',fechafinalizacion='2018-05-05', duracionsprint='15',administrador_id=User.objects.get(username='usuario2').id).save()
        Sprint(nombresprint='Sprint1',fechainicio='2018-03-12',fechafinalizacion=datetime.strptime('2018-03-12', "%Y-%m-%d").date()+ timedelta(14),duracion=15,proyecto_id_id=12).save()

    def test_crearsprint_error_2(self):
            User.objects.create_user(password='contrasena', username='usuario3', email='email@email.com', first_name='nombres',last_name='apellidos').save()
            Proyectos(nombreproyecto='nombreproyecto', codigoproyecto='11', anoasociado='2018', fechainicio='2018-03-12',fechafinalizacion='2018-05-05', duracionsprint='15',administrador_id=User.objects.get(username='usuario3').id).save()
            Sprint(nombresprint='Sprint1',fechainicio='2018-25-12',fechafinalizacion=datetime.strptime('2018-03-12', "%Y-%m-%d").date()+ timedelta(14),duracion=15,proyecto_id_id=11).save()

