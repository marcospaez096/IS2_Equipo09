from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

from gestionarproyecto.models import relacion_rol_proyecto_usuario, Proyectos
from gestionarsprint.models import relacion_sprint_user_rol
from gestionarus.models import progresoususer
from .formulario import loginform
#from .usuarios import Users
from django.contrib.auth import *
from django.contrib.auth.models import *
# Create your views here.


def hacer_logout(request):
    logout(request)
    return render(request,'login/logout.html')

def hacer_login(request):

        if(request.method=='GET'):
            if (request.user.is_authenticated):
                return HttpResponseRedirect('/home/' + request.user.username)
            else:
                return render(request, 'login/formulario.html', {})
        else:
            if request.method == 'POST':  # Si el formulario se envio
                form = loginform(request.POST)  # Un formulario virtual enlazado a los datos post
                if form.is_valid():  # All validation rules pass
                    username = form.cleaned_data['username']
                    password = form.cleaned_data['password']
                    user = authenticate(username=username,password=password)
                    if user is not None:
                        objeto_permiso = User.objects.get(username=username).get_all_permissions()
                        login(request,user)
                        return HttpResponseRedirect('/home/'+username)

                    else:
                        return render(request, 'login/error.html', {},status=404)

@login_required(login_url='/')
def mostrar_interfaz(request,usuario_nombre):
    objeto_permiso = User.objects.get(username=usuario_nombre).get_all_permissions()
    usuario = User.objects.get(username = usuario_nombre)
    roluserproyecto = relacion_rol_proyecto_usuario.objects.filter(user=usuario)
    return render(request, 'login/index.html',{"objeto_permiso":objeto_permiso,"usuario":usuario,"roluserproyecto":roluserproyecto})

