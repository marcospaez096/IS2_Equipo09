from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.context_processors import auth
from django.contrib.auth import  views as auth_views
import gestionarproyecto
from . import views
import gestionarus
urlpatterns = [
    url(r'^$', views.hacer_login, name='login'),
    url(r'^logout$',views.hacer_logout,name='logout_url'),
    url(r'^usuario/(?P<usuario_nombre>[\w-]+)/', include('gestionarproyecto.urls')),
    url(r'^usuario/(?P<usuario_nombre>[\w-]+)/',include('gestionarroles.urls')),
    url(r'^home/(?P<usuario_nombre>[\w-]+)/', views.mostrar_interfaz,name='index'),
    url(r'^usuario/(?P<usuario_nombre>[\w-]+)/gestionarusuarios/',include('gestionarusuarios.urls')),
    url(r'^gestionarinformacion/',include('gestionarinformacion.urls'))
]

