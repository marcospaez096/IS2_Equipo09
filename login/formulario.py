

from django import forms

#creating our forms
class loginform(forms.Form):
	#django gives a number of predefined fields
	#CharField and EmailField are only two of them
	#go through the official docs for more field details
	username = forms.CharField(label='username', max_length=100)
	password = forms.CharField(label='password', max_length=100)
