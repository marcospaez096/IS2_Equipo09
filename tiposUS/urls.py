from django.conf.urls import include, url
from django.contrib import admin

from tiposUS import  views


app_name = 'tiposUS'

urlpatterns = [
    url(r'^agregarv2/', views.CreaTypeUSView.as_view(), name='creartiposusv2'),
    url(r'^listadotiposus/', views.listar_typeUS, name='listadotiposus'),
    url(r'^editar/(?P<detailtipous_url>\w+)$', views.EditTypeUSView.as_view(), name='updatetipous')
    ]