from typing import Dict

from django import forms
from django.core.exceptions import NON_FIELD_ERRORS

from gestionarproyecto.models import Proyectos
from tiposUS.models import gestiontipoUS
from django.db.models import Q
from gestionarFlujos.models import Flujo

class CreatetypeUSForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CreatetypeUSForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

        self.fields['subestado'].widget.attrs['readonly']='readonly'

    class Meta:
        model = gestiontipoUS
        fields = ['nombretipus', 'flujo', 'estado', 'subestado', 'proyecto']
        help_texts = {
            'estado': 'Estado1,Estado2,Estado3'
        }
        error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': "%(field_labels)s no se pueden repetir",
            }
        }