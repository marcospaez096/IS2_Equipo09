from django.shortcuts import render
from django.views.generic.edit import CreateView,UpdateView
from django.views.generic.detail import DetailView

from tiposUS.models import gestiontipoUS
from gestionarFlujos.models import Estado
from tiposUS.forms import CreatetypeUSForm


from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from gestionarproyecto.models import  *
from django.shortcuts import get_object_or_404

def listar_typeUS(request,usuario_nombre,proyecto_nombre_url):
    objetoproyectoactual = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
    objeto_tipo_us = gestiontipoUS.objects.filter(proyecto=objetoproyectoactual)
    return render(request,'mostrartiposUS.html',{  "objeto_tipo_us":objeto_tipo_us,'proyecto_nombre_url':proyecto_nombre_url,"username":usuario_nombre})


#Vista para mostrar el formulario y guardar un tipoUS

class CreaTypeUSView(CreateView):
    template_name = 'addTypeUS.html'
    model = gestiontipoUS
    form_class = CreatetypeUSForm
    #se valida el formulario
    def form_valid(self, form,**kwargs):
        #se trae el objeto Flujo del formulario
        Objectypetus=form.save(commit=False)
        #se enlaza el flujo objeto a un proyecto
        Objectypetus.proyecto = get_object_or_404(Proyectos, nombreproyecto=self.kwargs.get('proyecto_nombre_url'))
        # se guarda el objeto flujo
        Objectypetus.save()

        return  HttpResponseRedirect(reverse('visualizarproyecto',kwargs={'proyecto_nombre_url':self.kwargs.get('proyecto_nombre_url'),'usuario_nombre':self.request.user.username}))


class EditTypeUSView(UpdateView):
    template_name='editTypeUS.html'
    model = gestiontipoUS
    success_url = reverse_lazy('courses:list')
    form_class = CreatetypeUSForm

    def get_object(self,**kwargs):
        objproyecto = get_object_or_404(Proyectos, nombreproyecto=self.kwargs.get('proyecto_nombre_url'))
        return gestiontipoUS.objects.get(nombretipus=self.kwargs.get('detailtipous_url'),proyecto=objproyecto)





    def form_valid(self, form,**kwargs):
        obje=form.save(commit=False)
        obje.proyecto=get_object_or_404(Proyectos, nombreproyecto=self.kwargs.get('proyecto_nombre_url'))
        #se guarda en la base de datos
        obje.save()
        #se muestra el tipous creado en la plantilla
        return HttpResponseRedirect(reverse('tiposUS:detailtiposus', kwargs={'proyecto_nombre_url':obje.proyecto,'detailtipous_url': form.cleaned_data['nombre'], 'usuario_nombre' : self.kwargs.get('usuario_nombre')},current_app='tiposUS'))

