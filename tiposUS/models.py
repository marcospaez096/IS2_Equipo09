from django.db import models
from gestionarFlujos.models import Flujo
from gestionarproyecto.models import  Proyectos
from django.contrib.postgres.fields import ArrayField
# Se crear el modelo de datos tipo de US con los atributos

class gestiontipoUS(models.Model):
    nombretipus=models.CharField(max_length=100,blank=False)
    flujo =  models.CharField(max_length=100)
    proyecto = models.ForeignKey(Proyectos, on_delete=False, blank=False)
    estado = ArrayField(models.CharField(max_length=100, editable=False, unique=True))
    subestado = ArrayField(models.CharField(max_length=100, editable=False, unique=True), default='Hacer,Haciendo,Hecho')

    class Meta:
        db_table = 'gestiontipous'
        unique_together = ('nombretipus','proyecto'),('estado','proyecto')

    def __str__(self):
        return u'{0}'.format(self.nombretipus,self.estado,self.proyecto)
