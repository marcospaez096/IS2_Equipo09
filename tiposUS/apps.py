from django.apps import AppConfig
from django.contrib import admin
from tiposUS.models import tipoUS

class TiposusConfig(AppConfig):
    name = 'tiposUS'

admin.site.register(tipoUS)
