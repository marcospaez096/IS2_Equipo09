import unittest
from login.usuarios import Users
from django.test import TestCase
#Client simula a un navegador
from django.test.client import Client
from django.contrib.auth.models import User
class SimplePrueba(unittest.TestCase):
    def test_pagina_login(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(response.status_code,200)

    def test_logueo(self):
        user = Users.objects.create(username='marco',password='1234')
        user.save()
        self.assertEqual(Users.objects.filter(username='marco',password='1234').exists(),True)

    def test_redireccion(self):
        c = Client()
        self.assertEqual(c.post('/comprobar', {'username': 'marcoeerewre', 'password': '1234'}, follow=True).status_code,404)

