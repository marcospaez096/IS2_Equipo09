echo "Cargando Script"

read -p "Ingrese el path donde sera guardado el proyecto de IS2: " path

echo "Sobreescribiendo el archivo /etc/apache2/sites-available/000-default.conf"

echo "
<VirtualHost *:80>

	Alias /static/ $path/IS2_Equipo09/static/
	
	<Directory $path/IS2_Equipo09/static>
		Require all granted
		 Allow from all 
	</Directory>

	Alias /media/ $path/IS2_Equipo09/media/
	<Directory /home/labhpc-20/Publico/IS2_Equipo09/media/>
		Require all granted
		Allow from all 
	</Directory>
	<Directory $path/IS2_Equipo09/IS2_SGP>
            ##Options Indexes FollowSymLinks Includes ExecCGI
            ##AllowOverride All
            ##Order allow,deny
            ##Allow from all
            Require all granted
        </Directory>

	<Directory $path/IS2_Equipo09/IS2_SGP>
		<Files wsgi.py>
			Require all granted
			 Allow from all

		</Files>
	</Directory>
	WSGIDaemonProcess IS2_SGP python-home=$path/IS2_Equipo09/venv python-path=$path/IS2_Equipo09
	WSGIProcessGroup IS2_SGP
	WSGIScriptAlias / $path/IS2_Equipo09/IS2_SGP/wsgi.py process-group=IS2_SGP
        WSGIApplicationGroup %{GLOBAL}


</VirtualHost> " > /etc/apache2/sites-available/000-default.conf

echo "Reiniciando apache con las configuraciones actuales"

sudo service apache2 restart

echo "Ajustes de Base de Datos"



