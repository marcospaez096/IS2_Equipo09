echo "Cargando Script"

read -p "Ingrese el path donde sera guardado el proyecto de IS2: " path

echo "Comprobando si existe el directorio"

if [ ! -d $path/IS2_Equipo09 ]; then
  echo "Creando directorio IS2_Equipo09"
  mkdir -p $path/IS2_Equipo09;
fi

echo "Clonando el repositorio en la direccion: $path "

sudo git clone https://gitlab.com/akito3/IS2_Equipo09 $path/IS2_Equipo09

echo "Cargando Lista de Tag hechos\n"

cd 

cd $path/IS2_Equipo09

echo "Inicializando Git" 

sudo git init

sudo ls *

sudo git fetch --all

echo "Mostrando Lista de Tags"

sudo git tag -l 

read -p "Ingrese el nombre del tag que quiere descargar : " nombre_tag

echo "Tagging $nombre_tag"

git checkout tags/$nombre_tag 


echo "Sobreescribiendo el archivo /etc/apache2/sites-available/000-default.conf"

echo "
<VirtualHost *:80>

	Alias /static/ $path/IS2_Equipo09/static/
	
	<Directory $path/IS2_Equipo09/static>
		Require all granted
		 Allow from all 
	</Directory>

	Alias /media/ $path/IS2_Equipo09/media/
	<Directory /home/labhpc-20/Publico/IS2_Equipo09/media/>
		Require all granted
		Allow from all 
	</Directory>
	<Directory $path/IS2_Equipo09/IS2_SGP>
            ##Options Indexes FollowSymLinks Includes ExecCGI
            ##AllowOverride All
            ##Order allow,deny
            ##Allow from all
            Require all granted
        </Directory>

	<Directory $path/IS2_Equipo09/IS2_SGP>
		<Files wsgi.py>
			Require all granted
			 Allow from all

		</Files>
	</Directory>
	WSGIDaemonProcess IS2_SGP python-home=$path/IS2_Equipo09/venv python-path=$path/IS2_Equipo09
	WSGIProcessGroup IS2_SGP
	WSGIScriptAlias / $path/IS2_Equipo09/IS2_SGP/wsgi.py process-group=IS2_SGP
        WSGIApplicationGroup %{GLOBAL}


</VirtualHost> " > /etc/apache2/sites-available/000-default.conf

echo "Reiniciando apache con las configuraciones actuales"

sudo service apache2 restart

echo "Ajustes de Base de Datos"




sudo -u postgres psql -c  "drop user if exists prod_sgp_bd;"
sudo -u postgres psql -c "CREATE USER is2grp09 PASSWORD 'proyecto2018';"
sudo -u postgres psql -c "DROP SCHEMA public CASCADE;"
sudo -u postgres psql -c "CREATE SCHEMA public;"
sudo -u postgres psql -c "CREATE DATABASE  sgp_bd;"
sudo -u postgres psql -c "grant usage on schema public to is2grp09"
sudo -u postgres psql -c "grant create on schema public to is2grp09"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE  sgp_bd to is2grp09;"


echo ' entorno de Desarrollo listo'

sudo -u postgres psql -c"CREATE DATABASE  produccion_sgp_bd;"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE  produccion_sgp_bd to is2grp09;"
echo ' entorno de Desarrollo listo'
echo ' entorno de Produccion listo'


python3 manage.py makemigrations --settings=IS2_SGP.produccion_settings
python3 manage.py migrate --settings=IS2_SGP.produccion_settings

echo 'Poblando datos'
python3 poblacion.py
echo 'datos cargados correctamente'

echo 'Iniciando entorno de produccion'
python3 manage.py runserver --settings=IS2_SGP.produccion_settings
echo "Fin del script"
