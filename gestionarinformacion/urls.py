from django.conf.urls import include, url
from django.contrib import admin

import gestionarinformacion
from . import views

urlpatterns = [
     url(r'^mostrarCopyright', views.redireccionar_mostrarCopyright, name="mostrarCopyright"),
]
