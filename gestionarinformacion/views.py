from django.shortcuts import render

# Create your views here.
def redireccionar_mostrarCopyright(request):
    username = request.GET.get('usuario')
    return render(request,'gestionarinformacion/mostrarCopyright.html',{'username':username})
