from aptdaemon.enums import _
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.contrib.auth.models import *
# Create your models here.
from django.db.models import NOT_PROVIDED

from django.contrib.auth.models import Permission




def get_permision_name(self):
    return self.name

Permission.add_to_class("__str__", get_permision_name)


STATUS_CHOICES = (
    ('En Curso', _("iniciado")),
    ('Inactivo', _("Inactivo")),
    ('Terminado', _("Terminado")),
)

class Proyectos(models.Model):
    nombreproyecto = models.CharField(max_length=100,unique=True)
    codigoproyecto= models.AutoField(primary_key=True,unique=True)
    anoasociado =  models.CharField(max_length=5)
    fechainicio = models.DateField()
    fechafinalizacion = models.DateField(null=True)
    duracionsprint = models.IntegerField()
    administrador_id = models.ForeignKey(User,on_delete=True)
    status = models.CharField(choices = STATUS_CHOICES, max_length=100,default='Inactivo')
    class Meta:
        managed= True
        db_table = 'proyectos'


    def __str__(self):
        return self.nombreproyecto


class permisosProyecto(models.Model):
    class Meta:
        db_table = 'permisosProyecto'
        permissions = (
            ('gest_US', 'Crear-Modificar-Eliminar-Asignar-US'),
            ('gest_ProdBackLog', 'Agregar-Asignar-Eliminar US a ProductBackLog'),
            ('gest_TypeUS', 'Crear-Modificar-Eliminar-Asignar-TypeUS'),
            ('gest_Team', 'Agregar-Asignar-Eliminar Roles al Team'),
            ('gest_Sprint', 'Iniciar Crear-Eliminar-Asignar US a Sprint')

        )

class relacion_rol_proyecto_usuario(models.Model):
    rol =  models.ForeignKey(Group, on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE,null=True)
    proyecto = models.ForeignKey(Proyectos,on_delete=models.CASCADE)

    def __str__(self):
        return u'{0}'.format(self.user.username)