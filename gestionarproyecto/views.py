from django.contrib.auth.models import User, Group, Permission
from django.db.models import QuerySet
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.test import Client
from django.urls import reverse_lazy
from guardian.shortcuts import *
from django.contrib import messages
from django.utils.safestring import mark_safe
from django.utils.dateparse import *
from .forms import *
from gestionarsprint.models import Sprint, relacion_sprint_user_rol
from gestionarus.models import UserStory, progresoususer
from tiposUS.models import gestiontipoUS
from .models import *
from gestionarroles.models import *
from django.contrib.auth.decorators import permission_required, login_required



from django.http import Http404







# Create your views here.
#@permission_required('gestionarusuarios.createdproy')
@login_required(login_url='/')
def redireccionar_a_crear_proyecto(request,usuario_nombre):
    if(request.user.is_superuser):
        username = usuario_nombre
        roles = Group.objects.all()
        if request.method == "POST":
                form = formulario_proyecto(request.POST, usuario=usuario_nombre)
                if form.is_valid():
                    proyecto = form.save(commit=False)
                    proyecto.anoasociado = int(proyecto.fechainicio.year)
                    if not proyecto.fechafinalizacion:
                        proyecto.fechafinalizacion = proyecto.fechainicio
                    proyecto.save()
                    return redirect('buscarproyecto' , usuario_nombre)
                else:
                    return render(request, 'gestionarproyecto/crearproyecto.html', {"username": usuario_nombre, "roles": roles, "objeto_permiso": User.objects.get(username=usuario_nombre).get_all_permissions(),"formulario": form})

        else:

            formulario = formulario_proyecto(usuario=usuario_nombre)
            return render(request, 'gestionarproyecto/crearproyecto.html',{"username":username,"roles":roles, "objeto_permiso":User.objects.get(username=username).get_all_permissions(),"formulario":formulario})
    else:
        raise Http404

@login_required(login_url='/')
def redireccionar_a_buscar_proyecto(request,usuario_nombre):
    username = usuario_nombre
    objeto_proyectos = Proyectos.objects.all()
    usuario = User.objects.get(username=username)
    usuarios_all = User.objects.all()
    permisos = Permission.objects.all()
    roles = Group.objects.all()
    relaciones = relacion_rol_proyecto_usuario.objects.filter(user=usuario).all()
    return render(request,'gestionarproyecto/panelproyectos.html',{"username":username,"objeto_proyectos" : objeto_proyectos,"usuario":usuario,"usuarios_all":usuarios_all,"permisos":permisos,"roles":roles,"relaciones": relaciones,"objeto_permiso":usuario.get_all_permissions()})

def submit_relacion_proyecto_roles_usuarios(request):
    idproyecto = request.POST.get('codigoproyecto')
    idusuario = request.POST.get('idusuario')
    idrol = request.POST.get('idrol')
    #print(idproyecto)
    nueva_fila_relacion_proyecto_usuario_rol = relacion_user_rol_proyecto_id(user_id=idusuario,group_id=idrol,proyecto_id=Proyectos.objects.get(codigoproyecto=idproyecto))
    nueva_fila_relacion_proyecto_usuario_rol.save()
    return HttpResponse('Success')

@login_required(login_url='/')
def redireccionar_a_modificar_proyecto(request,usuario_nombre,proyecto_nombre_url):
    username = usuario_nombre
    roles = Group.objects.all()
    if request.method == "POST":
            instancia = get_object_or_404(Proyectos, nombreproyecto=proyecto_nombre_url)
            form = formulario_proyecto(request.POST, usuario=usuario_nombre,instance=instancia)
            if form.is_valid():
                proyecto = form.save(commit=False)
                if not proyecto.fechafinalizacion:
                    proyecto.fechafinalizacion = proyecto.fechainicio
                proyecto.save()
                return redirect('buscarproyecto' , usuario_nombre)
            else:
                return render(request, 'gestionarproyecto/modificarproyecto.html', {"username": usuario_nombre, "roles": roles, "objeto_permiso": User.objects.get(username=usuario_nombre).get_all_permissions(),"formulario": form})

    else:

        #Instancia es necesaria para obtener un objeto proyecto de la bd y pasarle al form y cargarlo con los datos
        instancia = get_object_or_404(Proyectos,nombreproyecto=proyecto_nombre_url)
        formulario = formulario_proyecto(usuario=usuario_nombre,instance=instancia)
        return render(request, 'gestionarproyecto/modificarproyecto.html',{"username":username,"roles":roles, "objeto_permiso":User.objects.get(username=username).get_all_permissions(),"formulario":formulario})

@login_required(login_url='/')
def submit_relacionar_proyectos_permisos_usuarios(request):
    try:
            idproyecto = request.POST.get('codigoproyecto')
            idusuario = request.POST.get('idusuario')
            idrol = request.POST.get("idrol")
            rol = Group.objects.get(id=idrol)
            usuario = User.objects.get(id=idusuario)
            proyecto = Proyectos.objects.get(codigoproyecto=idproyecto)
            relacion_rol_proyecto_usuario.objects.create(rol=rol, user=usuario, proyecto=proyecto).save()
            for i in rol.permissions.all():
                assign_perm(i,usuario,proyecto)





            return HttpResponse('success')
    except Exception as e:
            return HttpResponse('error')

@login_required(login_url='/')
def visualizarproyecto(request,usuario_nombre,proyecto_nombre_url):
    if request.GET.get('iniciarproyecto'):
        proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
        proyecto.status="iniciado"
        proyecto.save()
    if request.GET.get('terminarproyecto'):
        flag = True;
        for i in UserStory.objects.filter(sprintdefinido__proyecto_id=Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)).all():
            if i.estado != "finalizado":
                flag = False

        if flag:
            proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
            proyecto.status = "Terminado"
            proyecto.save()
    username = usuario_nombre
   # codigoproyecto = request.GET.get('codigoproyecto_a_visualizar')
    proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
    sprints_all = Sprint.objects.filter(proyecto_id=proyecto)

    objeto_proyectos = Proyectos.objects.all()
    usuario = User.objects.get(username=username)
    usuarios_all = User.objects.all()
    permisos = Permission.objects.all()
    roles = Group.objects.all()
    relaciones = relacion_rol_proyecto_usuario.objects.filter(proyecto=proyecto).all()
    tipous_all = gestiontipoUS.objects.filter(proyecto=proyecto).all()

    return render(request,'gestionarproyecto/visualizarproyecto.html',{"username":username,"proyecto":proyecto,"objeto_proyectos" : objeto_proyectos,"usuario":usuario,"usuarios_all":usuarios_all,"permisos":permisos,"roles":roles,"relaciones": relaciones,"sprints_all":sprints_all,"tipous_all":tipous_all})
@login_required(login_url='/')

def agregarmiembro(request,usuario_nombre, proyecto_nombre_url):
     if request.method == 'POST':
        form = formulario_relacion_rol_proyecto_usuario(request.POST,proyecto_nombre_url=proyecto_nombre_url)
        if form.is_valid():
            rol = Group.objects.get(name=form.cleaned_data['rol'].name)
            usuario = User.objects.get(username=form.cleaned_data['user'].username)
            proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
            for i in rol.permissions.all():
                assign_perm(i, usuario, proyecto)

            miembro=form.save(commit=False)
            miembro.proyecto=proyecto
            miembro.save()

            return redirect('visualizarproyecto' , usuario_nombre,proyecto_nombre_url)
        else:
            return render(request, 'gestionarproyecto/agregarmiembro.html',
                          {"username": usuario_nombre, "nombreproyecto": proyecto_nombre_url, "form": form})
     else:

        form = formulario_relacion_rol_proyecto_usuario(proyecto_nombre_url=proyecto_nombre_url)
        return render(request,'gestionarproyecto/agregarmiembro.html', {"username" : usuario_nombre , "nombreproyecto" : proyecto_nombre_url, "form" : form})

@login_required(login_url='/')
def reporte(request,usuario_nombre,proyecto_nombre_url):
    objeto_permiso = User.objects.get(username=usuario_nombre).get_all_permissions()
    usuario = User.objects.get(username = usuario_nombre)
    proyecto = Proyectos.objects.get(nombreproyecto=proyecto_nombre_url)
    roluserproyecto = relacion_rol_proyecto_usuario.objects.get(user=usuario,proyecto=proyecto)
    userstories = UserStory.objects.filter(usuarioasignado=usuario)
    progreso = progresoususer.objects.filter(usuarioacargo=usuario,)
    sprints = Sprint.objects.filter(proyecto_id=proyecto)
    matriz = "[\"User Story\",\"Trabajado\",\"Restante\"],"
    for us in userstories:
        if us.tipous.proyecto.codigoproyecto == proyecto.codigoproyecto:
            pid = us.tipous.proyecto.codigoproyecto
            print(pid)
            fila = "[" + "\"" + str(us.nombre) + "(" + str(us.sprintdefinido.nombresprint)  + ")" + "\"" + "," + str(us.tiempotrabajado) + "," + str(us.tiempoestimado-us.tiempotrabajado) + "],"
            matriz += fila
    return render(request, 'gestionarproyecto/reporteusuario.html', {"objeto_permiso":objeto_permiso, "usuario":usuario, "roluserproyecto":roluserproyecto, "proyecto":proyecto,"progreso":progreso,"sprints": sprints,"matriz":matriz})
