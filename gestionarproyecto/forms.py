from time import strptime

from django import  forms
from django.contrib.admin import models
from django.utils.functional import keep_lazy

from .models import *

class formulario_relacion_rol_proyecto_usuario(forms.ModelForm):
    rol = forms.ModelChoiceField(queryset=Group.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = relacion_rol_proyecto_usuario
        fields = '__all__'

    def __init__(self,*args,**kwargs):
        self.proyecto_nombre_url = kwargs.pop('proyecto_nombre_url')
        super(formulario_relacion_rol_proyecto_usuario,self).__init__(*args,**kwargs)
        self.fields['user']=  forms.ModelChoiceField(queryset=User.objects.exclude(id__in=relacion_rol_proyecto_usuario.objects.filter(proyecto=Proyectos.objects.get(nombreproyecto=self.proyecto_nombre_url)).values('user_id')),widget=forms.Select(attrs={'class': 'form-control'}))
        self.fields['proyecto'] = forms.ModelChoiceField(queryset=Proyectos.objects.filter(nombreproyecto=self.proyecto_nombre_url),widget=forms.Select(attrs={'class': 'form-control'}))



class formulario_proyecto(forms.ModelForm):
    nombreproyecto = forms.CharField(max_length=100,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    fechainicio = forms.DateField(required=True,widget=forms.TextInput(attrs={'type':'date','class':'form-control'}))
    #fechafinalizacion = forms.DateField(required=True,widget=forms.TextInput(attrs={'type':'date','class':'form-control'}))
    duracionsprint = forms.IntegerField(required=True,widget=forms.TextInput(attrs={'class':'form-control','value':15}))
    #status = forms.ChoiceField(required=True,widget=forms.TextInput(attrs={'class':'form-control','readonly':True,'value': 'En Curso'}))

    #administrador_id = forms.ModelChoiceField(required=True,widget=forms.Select({'class':'form-control'}))
    class Meta:
        model =  Proyectos
        fields = '__all__'
        exclude = ['fechafinalizacion', 'anoasociado','status']

    def __init__(self, *args, **kwargs):
        # self.proyecto_nombre_url = kwargs.pop('proyecto_nombre_url')
        self.usuario = kwargs.pop('usuario')
        super(formulario_proyecto, self).__init__(*args, **kwargs)
        self.fields['administrador_id'] = forms.ModelChoiceField(queryset=User.objects.filter(username=self.usuario),widget=forms.Select(attrs={'class': 'form-control'}))
