from django.conf.urls import include, url
from django.contrib import admin

import gestionarproyecto
from . import views
import tiposUS
urlpatterns = [
    url(r'^gestionarproyecto/crearproyecto', views.redireccionar_a_crear_proyecto, name='crearproyecto'),
    url(r'^gestionarproyecto/buscarproyecto$',views.redireccionar_a_buscar_proyecto, name='buscarproyecto'),
    url(r'^gestionarproyecto/submit_relacion_proyecto_roles_usuarios', views.submit_relacion_proyecto_roles_usuarios,name='submit_relacion_proyecto_roles_usuarios'),
    url(r'^gestionarproyecto/(?P<proyecto_nombre_url>[\w\s]+)/modificarproyecto',views.redireccionar_a_modificar_proyecto,name='modificarproyecto'),
    url(r'^gestionarproyecto/(?P<proyecto_nombre_url>[\w\s]+)/reporte',views.reporte,name='reporte'),
    url(r'^gestionarproyecto/submit_relacionar_proyectos_permisos_usuarios',views.submit_relacionar_proyectos_permisos_usuarios,name="submit_relacionar_proyectos_permisos_usuarios"),
    url(r'^gestionarproyecto/(?P<proyecto_nombre_url>[\w\s]+)/visualizarproyecto',views.visualizarproyecto,name="visualizarproyecto"),
    url(r'^gestionarproyecto/(?P<proyecto_nombre_url>[\w\s]+)/tipous/', include('tiposUS.urls')),
    url(r'^gestionarproyecto/(?P<proyecto_nombre_url>[\w\s]+)/flujo/', include('gestionarFlujos.urls')),
    url(r'^gestionarproyecto/(?P<proyecto_nombre_url>[\w\s]+)/gestionarsprint/', include('gestionarsprint.urls')),
    url(r'^gestionarproyecto/(?P<proyecto_nombre_url>[\w\s]+)/gestionarus/', include('gestionarus.urls')),
    url(r'^gestionarproyecto/(?P<proyecto_nombre_url>[\w\s]+)/agregarmiembro/',views.agregarmiembro,name='agregarmiembro'),
    url(r'^gestionarproyecto/(?P<proyecto_nombre_url>[\w\s]+)/sprintbacklog/',include('gestionarSprintBackLog.urls'))
]
