from django.shortcuts import render
from django.shortcuts import render
from django.views.generic.edit import CreateView,UpdateView
from django.views.generic.detail import DetailView


from gestionarSprintBackLog.models import SprintBacklog
from gestionarSprintBackLog.forms import FormCreateSprintBackLog
# Create your views here.
class ViewCrearSprintBacklog(CreateView):
    template_name = 'addsprintbacklog.html'
    model = SprintBacklog
    form_class = FormCreateSprintBackLog