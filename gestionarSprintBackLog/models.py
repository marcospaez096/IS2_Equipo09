from django.db import models

from gestionarus.models import UserStory
from gestionarproyecto.models import Proyectos
from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple

class SprintBacklog(models.Model):
    proyecto=models.ForeignKey(Proyectos,on_delete=False, blank=False)
    nombre = models.CharField(max_length=100, unique=True)
    us = models.ManyToManyField(UserStory)

    def __str__(self):
        return self.nombre





