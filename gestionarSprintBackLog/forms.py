
from django import forms

from django.contrib.admin.widgets import FilteredSelectMultiple

from gestionarSprintBackLog.models import SprintBacklog
from gestionarus.models import UserStory

forms.MultipleChoiceField.widget = FilteredSelectMultiple("verbose name", is_stacked=False)


class FormCreateSprintBackLog(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FormCreateSprintBackLog, self).__init__(*args, **kwargs)


    class Meta:
        model = SprintBacklog
        fields =[ 'nombre','us']

    us=forms.ModelMultipleChoiceField(queryset=UserStory.objects.all(),widget=FilteredSelectMultiple("nombre",is_stacked=False,),)


    class Media:
            css = {
                'all': ('/media/css/widgets.css',),
            }
            js = ('/admin/jsi18n/',)

