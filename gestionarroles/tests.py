from django.test import TestCase
from gestionarproyecto.models import *
# Create your tests here.
import unittest
from django.test import TestCase
#Client simula a un navegador
from django.test.client import Client
from django.contrib.auth.models import User
from _socket import INADDR_MAX_LOCAL_GROUP
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import *
from django.contrib.auth import *
from django.contrib.auth.models import Group, Permission, User
from django.contrib.contenttypes.models import ContentType
class SimplePrueba(unittest.TestCase):

    def test_crearrol(self):

        Group.objects.create(name="is2")
        Group.objects.get(name="is2").delete()
        #Group.objects.get(name="is2")

    def test_crear_permisos(self):
        Group.objects.create(name="is")

        permission = Permission.objects.create(codename="permisotal",
                                                       name="poderhacertalcosa",
                                                       content_type=ContentType.objects.get_for_model(User))
        Group.objects.get(name="is").permissions.add(permission)
        User.objects.create_user(password='contrasena', username='hola', email='email@email.com', first_name='nombres',
                                 last_name='apellidos').save()

        Proyectos(nombreproyecto='nombreproyecto', codigoproyecto='11', anoasociado='2018', fechainicio='2018',
                  fechafinalizacion='2018', duracionsprint='15dias',
                  administrador_id=User.objects.get(username='hola').id).save()

        relacion_user_rol_proyecto_id(group_id=Group.objects.get(name="is").id,
                                      proyecto_id=Proyectos.objects.get(codigoproyecto='11'),
                                      user_id=User.objects.get(username='hola').id).save()

        print(Group.objects.get(name="is").permissions.get(codename="permisotal").codename)
        print(relacion_user_rol_proyecto_id.objects.get(user_id=User.objects.get(username="hola").id).group.permissions.get(codename="aaaasa").codename)
