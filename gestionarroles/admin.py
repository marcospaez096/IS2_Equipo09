from django.contrib import admin
from gestionarproyecto.models import  Proyectos
from gestionarroles.models import  relacion_user_rol_proyecto_id
from gestionarFlujos.models import Flujo,Estado
from tiposUS.models import gestiontipoUS
from gestionarSprintBackLog.models import SprintBacklog
# Register your models here.
admin.site.register(Proyectos)
admin.site.register(relacion_user_rol_proyecto_id)
admin.site.register(Flujo)
admin.site.register(Estado)

admin.site.register(gestiontipoUS)
admin.site.register(SprintBacklog)