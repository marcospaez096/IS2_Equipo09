from django import forms

from gestionarroles.models import relacion_user_rol_proyecto_id
from django.contrib.auth.models import Permission

from django.contrib.contenttypes.models import *
from django.db.models import Q
from gestionarFlujos.models import Flujo
class CrearRolesForm(forms.Form):
    nombrerol= forms.CharField(max_length=100,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    #permisos = forms.ModelMultipleChoiceField(queryset=Permission.objects.filter(content_type_id=ContentType.objects.get(model='permisosproyecto').id),widget=forms.CheckboxSelectMultiple)
