from email._header_value_parser import get_group

from django.db import models
from django.contrib.auth.models import User,Group
from django.db.models.signals import post_save
from django.dispatch import receiver
from gestionarproyecto.models import Proyectos



class relacion_user_rol_proyecto_id(models.Model):
        user = models.ForeignKey(User,on_delete=models.CASCADE)
        group = models.ForeignKey(Group,on_delete=models.CASCADE)
        proyecto_id = models.ForeignKey(Proyectos,on_delete=models.CASCADE)
        #def __str__(self):
            #    return  str(self.user)+str(self.proyecto_id)


