from _socket import INADDR_MAX_LOCAL_GROUP
from pydoc import describe

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from guardian.shortcuts import remove_perm, assign_perm

from .models import *
from django.contrib.auth import *
from django.contrib.auth.models import Group, Permission, User
from django.contrib.contenttypes.models import ContentType
from django.apps import apps
from gestionarproyecto.models import *
# Create your views here.
from django.urls import reverse


from gestionarroles.forms import CrearRolesForm

def redireccionar_a_crear_roles(request,usuario_nombre):
    return render(request,'gestionarroles/crearroles.html',{"username":usuario_nombre})

def rolesv2(request,usuario_nombre):
    if(request.method=='GET'):
        form=CrearRolesForm()
        return render(request, 'gestionarroles/crearrolesv2.html',{"form": form})
    else:
        form= CrearRolesForm(request.POST)
        if(form.is_valid()):
            nombrerol = form.cleaned_data['nombrerol']
            nuevogrupo = Group.objects.create(name=nombrerol)

            for perm in form.cleaned_data['permisos']:
                nuevogrupo.permissions.add(perm)
        return HttpResponseRedirect(reverse('mostrarroles',kwargs={'usuario_nombre':usuario_nombre}))


def modificarrolesv2(request,usuario_nombre,rol_a_modificar):
    if (request.method == 'GET'):
        permisoroles=Group.objects.get(name=rol_a_modificar)
        permisoroles=permisoroles.permissions.all()
        form=CrearRolesForm(initial={'nombrerol':rol_a_modificar,'permisos':permisoroles})
        return render(request, 'gestionarroles/modificarrolesv2.html', {'form': form})
    else:
        form = CrearRolesForm(request.POST)
        if (form.is_valid()):
            nombrerol = form.cleaned_data['nombrerol']
            nuevogrupo = Group.objects.get(name=rol_a_modificar)
            nuevogrupo.name=nombrerol
            nuevogrupo.permissions.clear()
            for perm in form.cleaned_data['permisos']:
                nuevogrupo.permissions.add(perm)
            nuevogrupo.save()
        return HttpResponseRedirect(reverse('mostrarroles', kwargs={'usuario_nombre': usuario_nombre}))




def submit_crear_roles(request,usuario_nombre):
    try:
        nombrerol = request.POST.get('nombrerol')
        permisos = request.POST.get('nombrepermisos')
        descripcionpermisos = request.POST.get('descripcionpermisos')
        ambitopermiso = request.POST.get('ambitopermiso').split(".")
        modelo = apps.get_model(ambitopermiso[0], ambitopermiso[1])
        nuevogrupo = Group.objects.create(name=nombrerol)
        array_string_permisos = permisos.split(",")
        array_string_descripcionpermisos = descripcionpermisos.split(",")
        for i in range(len(array_string_permisos)):
            if Permission.objects.filter(codename=array_string_permisos[i]).exists():
                nuevogrupo.permissions.add(Permission.objects.get(codename=array_string_permisos[i]))
            else:
                permission = Permission.objects.create(codename=array_string_permisos[i],
                                                   name=array_string_descripcionpermisos[i],
                                                   content_type=ContentType.objects.get_for_model(modelo))
                nuevogrupo.permissions.add(Permission.objects.get(codename=array_string_permisos[i]))

        return redirect('mostrarroles',usuario_nombre)
    except Exception as e:
        print(e)

        print('it works!')




def submit_editar_roles(request):
    nombrerol = request.POST.get('nombrerol')
    idrol = request.POST.get('idrol')
    permisos = request.POST.get('nombrepermisos')
    descripcionpermisos = request.POST.get('descripcionpermisos')
    permisos_antiguos = request.POST.get('nombrepermisosantiguos')
    grupo=Group.objects.get(id=idrol)
    grupo.name=nombrerol
    array_string_permisos = permisos.split(",")
    array_string_permisos_antiguos = permisos_antiguos.split(",")
    #print(array_string_permisos_antiguos)

    array_string_descripcionpermisos = descripcionpermisos.split(",")
    if len(array_string_permisos_antiguos) > len(array_string_permisos):
        grupo.permissions.clear()
        for x in range(len(array_string_permisos_antiguos)):

            permission = Permission.objects.get(codename=array_string_permisos_antiguos[x]).delete()

        for x in range(len(array_string_permisos)):
            permission = Permission.objects.create(codename=array_string_permisos[x],
                                                   name=array_string_descripcionpermisos[x],
                                                   content_type=ContentType.objects.get_for_model(User))
            grupo.permissions.add(permission)
    else:
            for i in range(0,len(array_string_permisos_antiguos)):
                         permission = Permission.objects.get(codename=array_string_permisos_antiguos[i])
                         permission.codename = array_string_permisos[i]
                         permission.name=array_string_descripcionpermisos[i]
                         permission.content_type=ContentType.objects.get_for_model(User)
                         permission.save()

                #grupo.permissions.add(permission)
            grupo.save()
            for z in range(i+1,len(array_string_permisos)):
                print('entre')

                newpermission = Permission.objects.create(codename=array_string_permisos[z],
                                                       name=array_string_descripcionpermisos[z],
                                                       content_type=ContentType.objects.get_for_model(User))
                grupo.permissions.add(newpermission)





def mostrar_roles(request,usuario_nombre):
    roles=Group.objects.all()
    username = usuario_nombre
    return render(request,'gestionarroles/mostrarroles.html',{"username":username,"roles":roles,"objeto_permiso":User.objects.get(username=username).get_all_permissions(),"relacion":relacion_rol_proyecto_usuario.objects.all()})

def modificarpermisos(request,usuario_nombre,rol_a_modificar):
    username = usuario_nombre
    rol = Group.objects.get(name=rol_a_modificar)
    permisos = rol.permissions.all()

    return render(request,'gestionarroles/editroles.html',{"username": usuario_nombre,'permisos' :permisos,"rol" : rol})

def redireccionar_a_crear_permisos(request):
    username = request.GET.get('usuario')
    return render(request,'gestionarroles/crearroles.html',{"username":username,"objeto_permiso":User.objects.get(username=username).get_all_permissions()})
def submit_crear_permisos(request):
    try:

            nombrepermiso = request.POST.get('nombrepermiso')
            descripcionpermiso = request.POST.get('descripcionpermiso')
            ambitopermiso = request.POST.get('ambitopermiso')
            modelo = apps.get_model('gestionarproyecto',ambitopermiso)
            nuevopermiso = Permission.objects.create(codename=nombrepermiso,name=descripcionpermiso,content_type=ContentType.objects.get_for_model(modelo))
            nuevopermiso.save()

    except Exception as e:
            print(e)

def submit_modificar_permisos(request,usuario_nombre):
    try:
            nombrerol = request.POST.get('nombrerol')
            rol = Group.objects.get(name=nombrerol)

            relaciones = relacion_rol_proyecto_usuario.objects.filter(rol=rol).all()
            if 'crear' not in request.POST:
                for i in rol.permissions.all():
                    if i.codename == 'crear':
                        rol.permissions.remove(i)
                        for w in relaciones:
                            remove_perm(i, User.objects.get(username=w.user),
                                         Proyectos.objects.get(codigoproyecto=w.proyecto_id))
            else:
                if not rol.permissions.filter(codename='crear').exists():
                        permiso = Permission.objects.get(codename='crear')
                        rol.permissions.add(permiso)
                        for w in relaciones:
                            assign_perm(permiso,User.objects.get(username=w.user),Proyectos.objects.get(codigoproyecto=w.proyecto_id))

            if 'eliminar' not in request.POST:
                for i in rol.permissions.all():
                    if i.codename == 'eliminar':
                        #rol.permissions.remove(i)
                        for w in relaciones:
                         remove_perm(i, User.objects.get(username=w.user), Proyectos.objects.get(codigoproyecto=w.proyecto_id))
            else:
                if not rol.permissions.filter(codename='eliminar').exists():
                    permiso = Permission.objects.get(codename='eliminar')
                    rol.permissions.add(permiso)
                    for w in relaciones:
                        assign_perm(permiso, User.objects.get(username=w.user),
                                    Proyectos.objects.get(codigoproyecto=w.proyecto_id))

            if 'visualizar' not in request.POST:
                for i in rol.permissions.all():
                    if i.codename == 'visualizar':
                            rol.permissions.remove(i)
                            for w in relaciones:
                                remove_perm(i, User.objects.get(username=w.user),
                                            Proyectos.objects.get(codigoproyecto=w.proyecto_id))
            else:
                if not rol.permissions.filter(codename='visualizar').exists():
                    permiso = Permission.objects.get(codename='visualizar')
                    rol.permissions.add(permiso)
                    for w in relaciones:
                        assign_perm(permiso, User.objects.get(username=w.user),
                                    Proyectos.objects.get(codigoproyecto=w.proyecto_id))

            if 'modificar' not in request.POST:
                for i in rol.permissions.all():
                    if i.codename == 'modificar':
                        rol.permissions.remove(i)
                        for w in relaciones:
                            remove_perm(i, User.objects.get(username=w.user),
                                        Proyectos.objects.get(codigoproyecto=w.proyecto_id))
            else:
                if not rol.permissions.filter(codename='modificar').exists():
                    permiso = Permission.objects.get(codename='modificar')
                    rol.permissions.add(permiso)
                    for w in relaciones:
                        assign_perm(permiso, User.objects.get(username=w.user),
                                    Proyectos.objects.get(codigoproyecto=w.proyecto_id))

    except Exception as e:
            print(e)

    return redirect('mostrarroles', usuario_nombre)