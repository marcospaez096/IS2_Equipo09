from django.conf.urls import include, url
from django.contrib import admin

import gestionarproyecto
from . import views

urlpatterns = [
    url(r'^crearrolesv2', views.rolesv2, name='crearrolesv2'),
    url(r'^crearroles', views.redireccionar_a_crear_roles, name='crearroles'),
    url(r'^submit_crear_roles', views.submit_crear_roles, name='submit_crear_roles'),
    url(r'^gestionarroles/mostrarroles', views.mostrar_roles, name='mostrarroles'),
    url(r'^submit_editar_roles', views.submit_editar_roles, name='submit_editar_roles'),
    #url(r'^gestionarroles/(?P<rol_a_modificar>[\w\s]+)/modificarpermisos',views.modificarrolesv2,name="modificarpermisos"),
    url(r'^gestionarroles/(?P<rol_a_modificar>[\w\s]+)/modificarpermisos',views.modificarpermisos,name="modificarpermisos"),

    url(r'^crearpermisos',views.redireccionar_a_crear_permisos,name="crearpermisos"),
    url(r'^submit_crear_permisos',views.submit_crear_permisos,name="submit_crear_permisos"),
    url(r'^submit_modificar_permisos',views.submit_modificar_permisos,name="submit_modificar_permisos"),

]
