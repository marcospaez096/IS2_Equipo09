"""
Requerimientos necesarios para correr el
 SISTEMA DE GESTION DE PROYECTOS


alabaster==0.7.10

Babel==2.5.3

certifi==2018.1.18

chardet==3.0.4

django-guardian==1.4.9

Django==2.0.3

docutils==0.14

idna==2.6

imagesize==1.0.0

Jinja2==2.10

Markdown==2.6.11

MarkupSafe==1.0

packaging==17.1

psycopg2==2.7.4

psycopg2-binary==2.7.4

Pycco==0.5.1

Pygments==2.2.0

pyparsing==2.2.0

pystache==0.5.4

pytz==2018.3

requests==2.18.4

six==1.11.0

smartypants==2.0.1

snowballstemmer==1.2.1

Sphinx==1.7.2

sphinxcontrib-websupport==1.0.1

urllib3==1.22
"""