from django.db import models
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User


# Create your models here.


class Usuario(models.Model):
        user = models.ForeignKey(User, on_delete=models.CASCADE)
        horas_laborales= models.IntegerField(default=12)
        horas_disponibles= models.IntegerField(default=12)
        residencia= models.CharField(max_length=100,null=True)
        telefono = models.CharField(max_length=100,null=True)


