from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import UserManager
from django.shortcuts import render, redirect
from .models import  *
from django.views.generic.edit import CreateView,UpdateView
from django.views.generic.detail import DetailView
from gestionarusuarios.forms import CreateUserForm, EditEditUserForm

from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.urls import reverse
from gestionarproyecto.models import  *
from django.shortcuts import get_object_or_404


from django.contrib.auth import *


from random import choice

def redireccionar_a_buscar_usuarios(request,usuario_nombre):
    username = usuario_nombre

    usuarios = User.objects.all()
    return render(request,'gestionarusuarios/buscarusuario.html',{"username":username,"objeto_usuario":usuarios,"objeto_permiso":User.objects.get(username=username).get_all_permissions()})



class CreaUserView(CreateView):
    template_name = 'gestionarusuarios/crearusuario.html'
    model = User
    form_class = CreateUserForm
    #se valida el formulario

    def password_random(self):
        longitud = 18
        valores = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%&+"
        p = ""
        p = p.join([choice(valores) for i in range(longitud)])
        return p

    def form_valid(self, form,**kwargs):
        usuario=form.save(commit=False)
        passw=self.password_random()
        print(passw)
        usuario.set_password(passw)
        usuario.save()
        return redirect('buscarusuarios',self.kwargs.get('usuario_nombre'))

class EditUserView(UpdateView):
    template_name = 'gestionarusuarios/modificarusuario.html'
    model = User
    success_url = reverse_lazy('courses:list')
    form_class = EditEditUserForm
    def get_object(self, **kwargs):
        return User.objects.get(username=self.request.GET.get('modificar'))

    def form_valid(self, form):
        usuario = form.save(commit=False)
        usuario.set_password(form.cleaned_data.get('password1'))
        usuario.save()
        return redirect('buscarusuarios', self.kwargs.get('usuario_nombre'))


