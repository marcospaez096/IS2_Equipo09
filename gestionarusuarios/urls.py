from django.conf.urls import include, url
from django.contrib import admin

import gestionarproyecto
from . import views

urlpatterns = [
    url(r'^crearusuario', views.CreaUserView.as_view(), name='crearusuario'),
    url(r'^buscarusuarios',views.redireccionar_a_buscar_usuarios,name="buscarusuarios"),
    url(r'^modificar_usuario',views.EditUserView.as_view(),name="modificar_usuario")
]
