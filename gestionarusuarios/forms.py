from django import forms
from django.forms import ModelForm

from gestionarproyecto.models import Proyectos
from gestionarusuarios.models import Usuario
from tiposUS.models import gestiontipoUS
from django.db.models import Q
from gestionarFlujos.models import Flujo
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
class CreateUserForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CreateUserForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model=User
        fields =['username', 'email']

class EditEditUserForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(EditEditUserForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model=User
        fields =['first_name', 'last_name', 'email','password1', 'password2']

class EditUsuario(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditUsuario, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model=Usuario
        fields ='__all__'
